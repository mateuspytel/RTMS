var APIKEYS = [];

module.exports = {
    addApiKey(apikey) {
        console.log('Adding key: ' + JSON.stringify(apikey));
        APIKEYS.push(apikey);
    },
    removeApiKey(apikey) {
        console.log('Removing apikey: ' + JSON.stringify(apikey));
        APIKEYS = APIKEYS.filter( key => key.key != apikey);
    },
    findApiKey(apikey, callback) {
        var key = APIKEYS.find( key => key.key === apikey);
        callback(key);
    }
};