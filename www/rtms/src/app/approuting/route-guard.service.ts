import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Injectable()
export class RouteGuardService implements CanActivate {
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.sessionStorage.retrieve('APIKEY')) {
      return true;
    }else {
      this.router.navigateByUrl('/login');
    }
  }

  constructor(private sessionStorage: SessionStorageService, private router: Router) { }

}
