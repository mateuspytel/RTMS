import { SensorComponent } from './../root/sensor/sensor.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../authorization/login/login.component';
import { RootComponent } from '../root/root/root.component';
import { SensorchartComponent } from '../root/charts/sensorchart/sensorchart.component';
import { PanelComponent } from '../root/panel/panel.component';
import { AddeditsensorComponent } from '../root/addeditsensor/addeditsensor.component';
import { UsersListComponent } from '../root/users/users/userslist.component';
import { AddedituserComponent } from '../root/users/addedituser/addedituser.component';
import { RouteGuardService } from './route-guard.service';
import { AlertlogsComponent } from '../root/alertlogs/alertlogs.component';
import { GeneratorComponent } from '../root/generator/generator.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'app', component: RootComponent, canActivate: [RouteGuardService], children: [
    {path: 'users-list', component: UsersListComponent},
    {path: 'users-list/manage/:context', component: AddedituserComponent},
    {path: 'sensors-list/manage/:context', component: AddeditsensorComponent},
    {path: 'sensors-list', component: SensorComponent},
    {path: 'sensor', component: SensorchartComponent},
    {path: 'panel', component: PanelComponent},
    {path: 'alerts-list', component: AlertlogsComponent},
    {path: 'charts', component: GeneratorComponent}
  ]},
  {path: '**', redirectTo: 'app/panel', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [RouteGuardService]
})
export class ApproutingRoutingModule { }
