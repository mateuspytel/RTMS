export class SensorResponse {
    message: string;
    data: Sensor[];
}

export class SensorDataResponse {
    message: string
    data: Sensor;
}

export class Sensor {
    _id: string;
    registered_at: number;
    type: string;
    name: string;
    localization: string;
    active: boolean;
    active_since: number;
    lastmodified: number;
    data: SensorData[];
    alerts: AlertData[];
    typeLabel: string;
    static getSensorLabel(type: string) {
        let label;
        Object.entries(SENSORTYPE).forEach(([key, value]) => {
            if (type === value.type) label = value.label;
        });
        return label;
    }
    static getSensorShorthandLabel(type: string) {
        let label;
        Object.entries(SENSORTYPE).forEach(([key, value]) => {
            if (type === value.type) label = value.shorthand;
        });
        return label;
    }
}

export class SensorData {
    date: number;
    value: number;
}

export class AlertData {
    _id: string;
    name: string;
    alert_type: string;
    threshold: number;
    receivers_list: string[]
}

export class SensorChartData {
    _id: string;
    serial_number: string;
    registered_at: number;
    type: string;
    typeLabel: string;
    name: string;
    localization: string;
    active: number;
    active_since: number;
    lastmodified: number;
    constructor(_id, registered_at, type, name, localization, active, active_since, lastmodified) {
        this._id = _id;
        this.registered_at = registered_at;
        this.type = type;
        this.name = name;
        this.localization = localization;
        this.active = active;
        this.active_since = active_since;
        this.lastmodified = lastmodified;
    }
}

export class SensorChartResponse {
    message: string;
    data: any;
}

export const SENSORTYPE = {
    TEMPERATURE: {type: 'temperature', text: 'Temperatura', label: 'Temperatura [\u1d52C]', shorthand: '[\u1d52C]', chart: 'AreaChart'},
    HUMIDITY: {type: 'humidity', text: 'Wilgotność', label: 'Wilgotność [%]', shorthand: '[%]', chart: 'AreaChart'},
    SMOKE: {type: 'smoke', text: 'Czujnik dymu', label: 'Stan [0/1]', shorthand: '[0/1]', chart: 'SteppedAreaChart'},
    FLOOD: {type: 'flood', text: 'Czujnik zalania', label: 'Stan [0/1]', shorthand: '[0/1]', chart: 'SteppedAreaChart'},
    VOLTAGE: {type: 'voltage', text: 'Woltomierz', label: 'Volt [V]', shorthand: '[V]', chart: 'AreaChart'}
}