export class UserResponse {
    message: string;
    data: User[];
}

export class UserDetailsResponse {
    message: string;
    data: User;
}

export class User {
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    created: number;
    lastLogin: number;
    active: string;
    role: string;
}

export class UserLoginResponse {
    message: string;
    user: { _id: string, firstname: string, lastname: string, role: string, apikey: string };
}

export const USER_ROLES = [
    { value: 'ROLE_ADMIN', label: 'Administrator'},
    { value: 'ROLE_TECH', label: 'Techniczny'},
    { value: 'ROLE_USER', label: 'Użytkownik'}
];