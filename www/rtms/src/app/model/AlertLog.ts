export class AlertLog {
    _id: string;
    sensor_id : string;
    sensor_type: string;
    localization: string;
    alert_name: string;
    alert_type: string;
    threshold: number;
    cancelled: boolean;
    date: number;
    receivers_list: string[];
}

export class AlertLogResponse {
    message: string;
    data: AlertLog[];
}