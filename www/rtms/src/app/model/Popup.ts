import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";

export class Popup {
    type: PopupType;
    message: string;
    private reaction = new Subject<boolean>();
    public getReaction(): Observable<boolean> {
        return this.reaction.asObservable();
    }
    accept(): void {
        this.reaction.next(true);
    }
    reject(): void {
        this.reaction.next(false);
    }
    constructor(type: PopupType, message: string) {
        this.type = type;
        this.message = message;
    }
}

export enum PopupType {
    OK,
    WARNING,
    ERROR,
    BLOCK
}