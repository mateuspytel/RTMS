import { Component, OnInit } from '@angular/core';
import { ViewChildren } from '@angular/core';
import { ValidatedinputComponent } from '../../root/validatedinput/validatedinput.component';
import { ApiService } from '../../apiservice/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChildren(ValidatedinputComponent) inputs: ValidatedinputComponent[];

  _user = {
    email: '',
    password: ''
  }

  submitLogin(): void {
    this.apiService.login(this._user).then( response => {
      console.log('Response: ' + JSON.stringify(response));
    })
    .catch( err => {
      this.inputs.forEach( input => input.validate(err));
    });
  }

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

}
