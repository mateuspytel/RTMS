import { Pipe, PipeTransform } from '@angular/core';
import { SENSORTYPE } from '../model/Sensor';

@Pipe({
  name: 'sensortypename'
})
export class SensortypenamePipe implements PipeTransform {

  transform(value: string, args?: any): any {
    let type = '';
    Object.entries(SENSORTYPE).forEach( ([key, entry]) => {
      if (value === entry.type) type = entry.text;
    });
    return type;
  }

}
