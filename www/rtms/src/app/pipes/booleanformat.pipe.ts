import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'booleanformat'
})
export class BooleanformatPipe implements PipeTransform {

  transform(value: boolean, args?: any): any {
    return (value === true) ? 'TAK' : 'NIE';
  }

}
