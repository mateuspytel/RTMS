import { Pipe, PipeTransform } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';

@Pipe({
  name: 'sensoridtoname'
})
export class SensoridtonamePipe implements PipeTransform {

  transform(value: string, args?: any): any {
    return this.ss.retrieve('sensors').find(s => s._id === value).name;
  }

  constructor(private ss: SessionStorageService) {}

}
