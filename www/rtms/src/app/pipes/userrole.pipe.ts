import { Pipe, PipeTransform } from '@angular/core';
import { USER_ROLES } from '../model/Users';

@Pipe({
  name: 'userrole'
})
export class UserrolePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let role = '';
    Object.entries(USER_ROLES).forEach( ([key, entry]) => {
      if (value === entry.value) role = entry.label;
    });
    return role;
  }

}
