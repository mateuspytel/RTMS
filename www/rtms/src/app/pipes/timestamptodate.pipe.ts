import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timestamptodate'
})
export class TimestamptodatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let date = 'yyyy-mm-dd hh:mm';
    if (value) {
      const time = new Date(value);
      date = time.getFullYear() + '-' + ('0' + (time.getMonth() + 1)).substr(-2) + '-' +
      ('0' + time.getDate()).substr(-2) + '\t' + time.getHours() + ':' + ('0' + time.getMinutes()).substr(-2);
    }
    return date;
  }

}
