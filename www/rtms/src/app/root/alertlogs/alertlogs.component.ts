import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../apiservice/api.service';
import { AlertLogResponse } from '../../model/AlertLog';

@Component({
  selector: 'app-alertlogs',
  templateUrl: './alertlogs.component.html',
  styleUrls: ['./alertlogs.component.css']
})
export class AlertlogsComponent implements OnInit {

  _alerts: AlertLogResponse;

  constructor(private apiservice: ApiService) { 
    this.apiservice.getAlertLogs().then( 
      alerts => { this._alerts = alerts; this._alerts.data.reverse(); }
     );
  }

  ngOnInit() {
  }

}
