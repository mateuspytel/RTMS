import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertlogsComponent } from './alertlogs.component';

describe('AlertlogsComponent', () => {
  let component: AlertlogsComponent;
  let fixture: ComponentFixture<AlertlogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertlogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertlogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
