import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../apiservice/api.service';
import { StorageService } from '../../storageservice/storage.service';
import { Sensor, SensorData, AlertData, SensorDataResponse } from '../../model/Sensor';
import { AlertLog } from '../../model/AlertLog';
import { PopupService } from '../popup/popup.service';
import { PopupType } from '../../model/Popup';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core';
import { WebsocketService } from '../../websocket/websocket.service';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit, OnDestroy {

  @ViewChild('chart') chart;

  _role: string;

  _sensor: Sensor;

  _alerts: AlertData[];

  _activeAlerts: AlertLog[];

  _ready = false;

  _now: any;

  _routeSubscription: Subscription;

  /*
  * Gooogle chart config
  */  
  _data = {
    chartType : 'ColumnChart',
    dataTable : [],
    seriesType : 'bars',
    options: {
      height: 300,
      legend: {position: 'none'},
      hAxis: {
        title: 'Czas',
        textStyle: {
          fontSize: 10
        },
        titleTextStyle: {
          italic: false,
          fontSize: 12
        }
      },
      vAxis: {
        title: '',
        minValue: 0,
        maxValue: 100,
        textStyle: {
          fontSize: 10
        },
        titleTextStyle: {
          italic: false,
          fontSize: 12
        }
      },
      pointSize: 4,
      colors: ['#55ACEF'],
      animation: {
        duration: 500,
        easing: 'in',
        startup: true
      }
    }
  }

  private getTimeFromTimestamp(timestamp: number): any {
    const time = new Date(timestamp);
    const h = time.getHours();
    const m = time.getMinutes();
    return { label: (h + ':' + ('0' + m).substr(-2)), value: (h * 60 + m)};
  }

  private setSingleDayChartData(data: SensorData[]): any {
    let timeTable = [];
    timeTable.push(['Czas', Sensor.getSensorLabel(this._sensor.type)]);
    data.forEach( time => {
      const t = this.getTimeFromTimestamp(time.date);
      timeTable.push([t.label, Number(time.value)]);
    });
    return timeTable;
  }

  private cancelAlert(id: string): void {
    this.popupService.showDecisionPopup(PopupType.WARNING, 'Odwołać alert?').getReaction().subscribe( response => {
      if (response === true) {
        this.apiService.cancelAlert(id).then( res => {
          if (res === true) {
            this.popupService.clearPopup();
            this.apiService.getActiveAlerts(this._sensor._id).then( res => {
              this._activeAlerts = res.data;
            });
            this.apiService.getSensorsAlerts(this._sensor._id).then( res => {
              this._sensor.alerts = res.data.alerts;
            });
          }
        });
      }else {
        this.popupService.clearPopup();
      }
    });
  }

  constructor(private apiService: ApiService, private activatedRoute: ActivatedRoute, 
              private local: LocalStorageService, private popupService: PopupService, private socket: WebsocketService) {
    this._routeSubscription = this.activatedRoute.queryParams.subscribe( params => {
      this._role = this.apiService.getUserRole();

      const id = params['_id'];
      if (id) {
        this.prepareComponentData(id);
      }else if (this.local.retrieve('LIVE_SENSOR')) {
        var last_id = this.local.retrieve('LIVE_SENSOR');
        this.prepareComponentData(last_id);
      }else {
        this._ready = true;
      }
    });

    setInterval(() => { this._now = Date.now(); }, 1000);
  }

  private prepareComponentData(_id: string) {
    // Retrieve sensor details and data from last 2 hours
    this.apiService.getSensorsDetails(_id).then( sensor_details => {
      this._sensor = sensor_details.data;
      this._sensor.typeLabel = Sensor.getSensorShorthandLabel(this._sensor.type);
      this._data.options.vAxis.title = Sensor.getSensorLabel(this._sensor.type);

      this.apiService.getSensorsDataFromRange(_id, this._sensor.lastmodified - (120 * 60 * 1000), this._sensor.lastmodified).then( data => {
        this._sensor.data = data.data.data;
        this._data.dataTable =  this.setSingleDayChartData(this._sensor.data);
        this._sensor.data.reverse(); 
        const max_value = Math.max.apply(Math, this._sensor.data.map(function(o) { return o.value; }));
        this._data.options.vAxis.maxValue = max_value + (0.3 * max_value);
        if (this.chart) {
          this.chart.wrapper.setDataTable(this._data.dataTable);
          this.chart.redraw();
        } 
        this._ready = true;
      });

      // Get active sensor alerts
      this.apiService.getActiveAlerts(this._sensor._id).then( result => { this._activeAlerts = result.data; });

      // Websocket changes
      this.subscribeToLiveSocketData(this._sensor._id);
    })    
  }

  subscribeToLiveSocketData(_id: string): void {
    // Subscribe to live chart data 
    this.socket.subscribeToSensorsLiveData(_id).subscribe( data => {
      console.log('Received socet data: ' + JSON.stringify(data));
      if (this._data.dataTable) {
        let table = this._data.dataTable;
        if (data.value && this.chart) {
          table.splice(1,1);
          table.push([this.getTimeFromTimestamp(data.date).label, data.value]);
          this._data.dataTable = table;
          this._sensor.data.pop();
          this._sensor.data.unshift(<SensorData>{ date: data.date, value: data.value});
          this.chart.wrapper.setDataTable(table);
          this.chart.redraw();
        }
      }
    });

    // Subscribe to live alert data
    this.socket.subscribeToAlertData().subscribe( alert => {
      this.apiService.getActiveAlerts(this._sensor._id).then( result => {
        this._activeAlerts = result.data;
      });
      this.apiService.getSensorsAlerts(this._sensor._id).then( result => {
        this._sensor.alerts = result.data.alerts;
        console.log('Alerts: ' + JSON.stringify(result.data.alerts));
      });
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    if (this._routeSubscription) this._routeSubscription.unsubscribe();
  }

}
