import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../apiservice/api.service';
import { SensorResponse, Sensor } from '../../model/Sensor';
import { Router } from '@angular/router';
import { StorageService } from '../../storageservice/storage.service';
import { PopupService } from '../popup/popup.service';
import { PopupType } from '../../model/Popup';

@Component({
  selector: 'app-sensor',
  templateUrl: './sensor.component.html',
  styleUrls: ['./sensor.component.css']
})
export class SensorComponent implements OnInit {

  _sensors: SensorResponse;

  _role: string;

  showSensorDetails(sensor: Sensor) {
    this.storageService.saveSensorsDetails(sensor);
    this.router.navigateByUrl(('/app/sensor?_id=' + sensor._id));
  }

  editSensor(id: string) {
    this.router.navigateByUrl('/app/sensors-list/manage/edit?_id=' + id);
  }

  toggleActivateSensor(id: string) {
    const _sensor = this._sensors.data.find( sensor => sensor._id === id);
    if (_sensor) {
      _sensor.active = !_sensor.active;
      this.apiService.updateSensorDetails(_sensor).then(() => {
        this.apiService.getSensorsList().then( sensors => {
          this._sensors = sensors;
        })
      }).catch(err => {
        _sensor.active = !_sensor.active;
        if (err) this.alertService.showPopup(PopupType.ERROR, 'Sensor zarejestrowany poprzez numer seryjny. Uzupełnij szczegóły dotyzcące czujnika');
      });
    }
  }

  constructor(private activated: ActivatedRoute, private alertService: PopupService, private apiService: ApiService, private router: Router, private storageService: StorageService) {
    this._role = this.apiService.getUserRole();
    this.activated.params.subscribe( params => {
      this.apiService.getSensorsList().then( sensors => {
        console.log('Fething sensors!');
        this._sensors = sensors;
      })
      .catch(err => {
        console.log('Error! No sensors fetched!');
      });
    });
  }

  ngOnInit() {
  }

}
