import { Injectable } from '@angular/core';
import { Popup, PopupType } from '../../model/Popup';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PopupService {

  _subject = new Subject<Popup>();

  subscribePopupMessages(): Observable<Popup> {
    return this._subject.asObservable();
  }

  unsubscribePopupMessages(): void {
    this._subject.unsubscribe();
  }

  showPopup(type: PopupType, message: string): void {
    this._subject.next(new Popup(type, message));
  }

  showDecisionPopup(type: PopupType, message: string): Popup {
    const popup = new Popup(type, message)
    this._subject.next(popup);
    return popup;
  }

  clearPopup(): void {
    this._subject.next();
  }

  constructor() { }

}
