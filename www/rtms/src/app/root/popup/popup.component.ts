import { Component, OnInit } from '@angular/core';
import { Popup } from '../../model/Popup';
import { PopupService } from './popup.service';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit, OnDestroy {

  _popup: Popup;
  _popupSubscription: Subscription;

  accept(): void {
    this._popup.accept();
  }

  reject(): void {
    this._popup.reject();
  }

  hidePopup() {
    this.popupService.clearPopup();
  }

  constructor(private popupService: PopupService) {
    this._popupSubscription = this.popupService.subscribePopupMessages().subscribe( popup => {
      this._popup = popup;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this._popupSubscription.unsubscribe();
  }
}
