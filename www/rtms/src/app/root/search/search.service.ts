import { Injectable } from '@angular/core';
import { ApiService } from '../../apiservice/api.service';
import { Sensor } from '../../model/Sensor';

@Injectable()
export class SearchService {

  findSensor(phrase: string): Promise<Sensor[]> {
    return new Promise((resolve, reject) => {
      this.apiservice.findSensorByPhrase(phrase).subscribe( 
        response => {
          if (response) {
            resolve(response.data);
          }
        },
        err => {
          reject(err);
        }
      );
    });
  }

  constructor(private apiservice: ApiService) { }

}
