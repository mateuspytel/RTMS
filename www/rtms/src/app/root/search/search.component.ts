import { Component, OnInit } from '@angular/core';
import { SearchService } from './search.service';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { Sensor } from '../../model/Sensor';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchPhrase: FormControl = new FormControl();
  _results: Sensor[];
  _focus = false;

  openSensorInPanel(id: string) {
    this.router.navigateByUrl('/app/panel?_id=' + id);
  }

  blur() {
    setTimeout(() => {
      this._focus = false;
    }, 200);
  }

  constructor(private searchService: SearchService, private router: Router) { 
    this.searchPhrase.valueChanges.debounceTime(200).subscribe( phrase => this.searchService.findSensor(phrase).then( result => {
      this._results = result;
    })
    .catch( err => {
      console.log('Error');
    }));
  }

  ngOnInit() {
  }

}
