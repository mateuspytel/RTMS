import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorchartComponent } from './sensorchart.component';

describe('SensorchartComponent', () => {
  let component: SensorchartComponent;
  let fixture: ComponentFixture<SensorchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
