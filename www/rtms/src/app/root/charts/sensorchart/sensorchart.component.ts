import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../apiservice/api.service';
import { Sensor, SensorResponse, SensorData, SensorDataResponse, SensorChartData, SENSORTYPE } from '../../../model/Sensor';
import { ActivatedRoute } from '@angular/router';
import { ChartReadyEvent } from 'ng2-google-charts';
import { IMyDpOptions, IMyDate } from 'mydatepicker';
import { NouiFormatter } from 'ng2-nouislider';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

export class TimeFormatter implements NouiFormatter {
  to(value: number): string {
    let h = (value <= 1440) ? Math.floor(value / 60) : (Math.floor(value / 60) - 24);
    let m = Math.floor(value % 60);
    let values = [h, m];
    let timeString: string = '';
    values.forEach((_, i) => {
      if(values[i] < 10) {
        timeString += '0';
      }
      timeString += values[i].toFixed(0);
      if(i < 1) {
        timeString += ':';
      }
    });
    return timeString;
  };

  from(value: string): number {
    let v = value.split(':').map(parseInt);
    let time: number = 0;
    time += v[0] * 3600;
    time += v[1] * 60;
    return time;
  }
}

@Component({
  selector: 'app-sensorchart',
  templateUrl: './sensorchart.component.html',
  styleUrls: ['./sensorchart.component.css']
})
export class SensorchartComponent implements OnInit {

  @ViewChild('chart') cchart;
  
  _sensor: SensorChartData;
  _sensorDataEmpty = false;
  _ready = false;
  _loadingReady = false;
  _sliderDisabled = true;
  _datepickerOptions: IMyDpOptions = {
    dateFormat: 'dd.mm.yyyy'
  };

  _dateFrom = {date: {year: 0, month: 0, day: 0}};
  _dateTo = {date: {year: 0, month: 0, day: 0}};

  _sliderRange = [0,1440];

  _timeTableValues = [];
  _modelChange = false;

  _sliderMin = 0;
  _sliderMax = 1440;
  _sliderConfig: any = {
    start: 1440 / 2,
    step: 1,
    connect: [false, true, false],
    tooltips: [new TimeFormatter(), new TimeFormatter()]
  }

  /*
  * Gooogle chart config
  */  
  _data = {
    chartType : 'AreaChart',
    dataTable : [],
    options: {
      height: 400,
      hAxis: {
        title: 'Czas',
        textStyle: {
          fontSize: 10
        },
        titleTextStyle: {
          italic: false,
          fontSize: 12
        }
      },
      vAxis: {
        title: '',
        minValue: 0,
        maxValue: 100,
        textStyle: {
          fontSize: 10
        },
        titleTextStyle: {
          italic: false,
          fontSize: 12
        }
      },
      pointSize: 4,
      colors: ['#55ACEF']
      /*animation: {
        duration: 500,
        easing: 'in',
        startup: true
      }*/
    }
  }

  saveLiveData(): void {
    this.local.store('LIVE_SENSOR', this._sensor._id);
    this.router.navigateByUrl('/app/panel?_id=' + this._sensor._id);
  }

  /*
  * Compare IMyDate
  */
  areIMyDatesEqual(first: IMyDate, sec: IMyDate) {
    return (first.year === sec.year && first.month === sec.month && first.day === sec.day) ? true : false;
  }

  /*
  * Set date range
  */
  setDateRange(): void {
    const start = this.getMyDateFromTimestamp(this._sensor.active_since);
    const end = this.getMyDateFromTimestamp(this._sensor.lastmodified);

    if (this.IMyDateToTimestamp(this._dateFrom.date) > this.IMyDateToTimestamp(this._dateTo.date)) {
      this._dateFrom = this._dateTo;
    }

    if (this.IMyDateToTimestamp(this._dateTo.date) < this.IMyDateToTimestamp(this._dateFrom.date)) {
      this._dateTo = this._dateFrom;
    }

    if (this.areIMyDatesEqual(this._dateFrom.date, start)) {
      this.getSensorsDataFromRange(this._sensor._id, this._sensor.active_since, (this.IMyDateToTimestamp(this._dateTo.date) + 86340000));
    } else if(this.areIMyDatesEqual(this._dateTo.date, end)) {
      this.getSensorsDataFromRange(this._sensor._id, this.IMyDateToTimestamp(this._dateFrom.date), this._sensor.lastmodified);
    } else if (this.areIMyDatesEqual(this._dateFrom.date, this._dateTo.date)) {
      this.getSensorsDataFromRange(this._sensor._id, this.IMyDateToTimestamp(this._dateFrom.date), (this.IMyDateToTimestamp(this._dateFrom.date) + 86340000));
    } else {
      this.getSensorsDataFromRange(this._sensor._id, this.IMyDateToTimestamp(this._dateFrom.date), (this.IMyDateToTimestamp(this._dateTo.date) - (60 * 1000)));
    }
  }

  /*
  * Date from changed
  */
  dateChanged(event) {
    this.setDateRange();
  }

  /*
  * Convert UNIX timestamp to IMyDate object for calendar component
  */
  private getMyDateFromTimestamp(timestamp: number): IMyDate {
    const time = new Date(timestamp);
    return {year: time.getFullYear(), month: time.getMonth() + 1, day: time.getDate()};
  }

  /*
  * Convert IMyDate object to timestamp
  */
  private IMyDateToTimestamp(date: IMyDate): number {
    return new Date(date.year, date.month - 1, date.day).getTime();
  }

  /*
  * Get well formated time label from minutes eg. 1440 => 24:00
  */
  private getTimeLabel(time: number): string {
    return Math.floor(time / 60) + ':' + ('0' + (Math.floor(time % 60))).substr(-2);
  }

  /*
  * Prepare chart data table when using slider
  */
  private updateChartDatatable(from: number, to: number): any {
    // Prepare updated chart data table
    const time_formated = [];
    // Calculate range indexes for array split
    const from_index = Math.floor((1 - ((this._sliderMax - from) / (this._sliderMax - this._sliderMin))) * this._timeTableValues.length);
    const to_index = Math.floor((1 - ((this._sliderMax - to) / (this._sliderMax - this._sliderMin))) * this._timeTableValues.length);
    // Set data table header
    time_formated.push(['Czas', Sensor.getSensorLabel(this._sensor.type)]);
    // Prepare chart data with labels and values
    for (let i = from_index; i < to_index; i++) {
      time_formated.push([this._timeTableValues[i].time, this._timeTableValues[i].value]);
    }
    this._modelChange = false;
    return time_formated;
  }

  /*
  * Change time range
  */
  changeTimeRange(event): void {
    this._modelChange = true;
    setTimeout(() => {
      if (this._modelChange && this._loadingReady && this._timeTableValues.length > 0) {
        this.cchart.wrapper.setDataTable(this.updateChartDatatable(this._sliderRange[0], this._sliderRange[1]));    
        this.cchart.redraw();
      }
    }, 500);
  }

  /*
  * Get time object from UNIX timestamp e.g.
    {
      label: '13:07',
      value: 23.5
    }
  */
  private getTimeFromTimestamp(timestamp: number): any {
    const time = new Date(timestamp);
    const h = time.getHours();
    const m = time.getMinutes();
    return { label: (h + ':' + ('0' + m).substr(-2)), value: (h * 60 + m)};
  }

  /*
  * Get well formated data label from UNIX time
  */
  private getDateLabel(timestamp: number): string {
    const time = new Date(timestamp);
    return time.getFullYear() + '-' + ('0' + (time.getMonth() + 1)).substr(-2) + '-' +
           ('0' + time.getDate()).substr(-2) + ' ' + ('0' + time.getHours()).substr(-2) + ':' + ('0' + time.getMinutes()).substr(-2);
  }

  private getSensorsDataFromRange(id: string, timestamp_from: number, timestamp_to: number): void {
    this._loadingReady = false;
    this.apiService.getSensorsDataFromRange(id, timestamp_from, timestamp_to)
    .then( sensor => {
      this.prepareChartData(sensor).then( chart_data => {
        this.cchart.wrapper.setDataTable(chart_data);
        this.cchart.redraw();
        setTimeout(() => { this._loadingReady = true; }, 2500);
      });
    })
    .catch( err => {
      // Handle error
    });
  }

  private isWithinOneDay(from: number, to: number): boolean {
    return (Math.abs(to - from) <= (24 * 60 * 60 * 1000)) ? true : false;
  }

  private getSensorStep(data: SensorData[]): number {
    return (Math.abs(data[1].date - data[0].date) / 1000 / 60);
  }

  private setSingleDayChart(data: Sensor): Promise<any> {
    return new Promise((resolve, reject) => {
      // Prepare sensor details information
      this._sensor.typeLabel = Sensor.getSensorShorthandLabel(this._sensor.type);
      // Set initial hour on slider
      let initial_hour = this.getTimeFromTimestamp(data.data[0].date).value;
      // Set finish hour on slider
      let last_hour = this.getTimeFromTimestamp(data.data[data.data.length - 1].date).value;
      // Enable slider
      this._sliderDisabled = false;
      // Disable calendar range
      this._datepickerOptions.disableUntil = { year: this._dateFrom.date.year, month: this._dateFrom.date.month, day: this._dateFrom.date.day};
      this._datepickerOptions.disableSince = { year: this._dateTo.date.year, month: this._dateTo.date.month, day: this._dateTo.date.day};

      // Set slider range
      this._sliderMin = initial_hour;
      this._sliderMax = last_hour;
      this._sliderRange = [initial_hour, last_hour];

      // Set maximum value on V-Axis 
      const maximum = Math.max.apply(Math, data.data.map(function(o) { return o.value; }));
      this._data.options.vAxis.maxValue = maximum + (0.3 * maximum);
      // Everything is ready to draw a chart - resolve true
      resolve(this.setSingleDayChartData(data.data));
    });
  }

  private setSingleDayChartData(data: SensorData[]): any {
    let timeTable = [];
    timeTable.push(['Czas', Sensor.getSensorLabel(this._sensor.type)]);
    data.forEach( time => {
      const t = this.getTimeFromTimestamp(time.date);
      timeTable.push([t.label, Number(time.value)]);
      this._timeTableValues.push({time: t.label, value: time.value});
    });
    return timeTable;
  }

  private setMultipleDaysChart(data: Sensor): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      // Prepare sensor details information
      this._sensor.typeLabel = Sensor.getSensorShorthandLabel(this._sensor.type);
      // Disabled slider
      this._sliderDisabled = true;
      const maximum = Math.max.apply(Math, data.data.map(function(o) { return o.value; }));
      this._data.options.vAxis.maxValue = maximum + (0.3 * maximum);
      // Everything is ready for drawing chart - resolve true
      resolve(this.setMultipleDaysChartData(data.data));
    })
  }

  private setMultipleDaysChartData(data: SensorData[]): any {
      let chart_data = [];
      chart_data.push(['Czas', Sensor.getSensorLabel(this._sensor.type)]);
      data.forEach( time => {
        chart_data.push([this.getDateLabel(time.date), time.value]);
      });
      return chart_data;
  }

  private prepareChartData(sensor: SensorDataResponse): Promise<any> {
    return new Promise((resolve, reject) => {
      if (sensor.data.data.length > 0) {
          // Check if dates diffrence is less than 24 hours 
          if (this.isWithinOneDay(sensor.data.data[0].date, sensor.data.data[sensor.data.data.length - 1].date)) {
            this.setSingleDayChart(sensor.data).then( response => {
              resolve(response);
            });
          }else {
            this.setMultipleDaysChart(sensor.data).then( response => {
              resolve(response);
            });
          }
      } else {
        reject(false);
      }
    });
  }
 
  constructor(private apiService: ApiService, private activatedRoute: ActivatedRoute, private local: LocalStorageService, private router: Router) {
    this.activatedRoute.queryParams.subscribe( params => {
      // Get URL parameters
      let sensor_id = params['_id'];
      // Retrieve sensor data from apiservice when route is activated
      this.apiService.getSensorsData(sensor_id)
      .then(
        response => {
          this._sensor = new SensorChartData(response.data._id, response.data.registered_at, response.data.type, response.data.name, response.data.localization,
          response.data.active, response.data.active_since, response.data.lastmodified);
          this.prepareChartData(response).then( chart_data => {
            // Set initial date on calendar 
            this._dateFrom.date = this.getMyDateFromTimestamp(response.data.active_since);
            // Set finish date on calendar
            this._dateTo.date = this.getMyDateFromTimestamp(response.data.lastmodified);
            // Prepare calendar day range
            this._datepickerOptions.disableUntil = { year: this._dateFrom.date.year, month: this._dateFrom.date.month, day: this._dateFrom.date.day - 1};
            this._datepickerOptions.disableSince = { year: this._dateTo.date.year, month: this._dateTo.date.month, day: this._dateTo.date.day + 1};
            // Set chart type 
            Object.entries(SENSORTYPE).forEach(([key, value]) => {
              if (this._sensor.type === value.type) { this._data.chartType = value.chart; }
            });
            // Prepare chart datatable
            this._data.dataTable = chart_data;
            this._data.options.vAxis.title = Sensor.getSensorLabel(this._sensor.type);
            this._ready = true;
            this._loadingReady = true;
          })
          .catch(err => {
            if (err === false) {
              this._sensorDataEmpty = true;
              this._ready = true;
            }
          });
        }
      )
      .catch(
        err => {
          if (err) {
            console.log('Error during geting sensor data');
          }
        }
      );
    });
  }

  ngOnInit() {
  }

}
