import { Injectable } from '@angular/core';
import { ApiService } from '../../apiservice/api.service';
import { User, UserResponse } from '../../model/Users';

@Injectable()
export class UserService {

  createUser(user: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.apiService.createUser(user).subscribe( response => {
        if (response) {
          (response.message === 'OK') ? resolve(true) : resolve(false);
        }
      },
      err => {
        if (err) {
          reject(err.error);
        }
      });
    })
  }

  blockUser(id: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.apiService.blockUser(id).subscribe( resposne => {
        if (resposne) {
          (resposne.message === 'OK') ? resolve(true) : resolve(false);
        }
      },
      err => {
        if (err) {
          reject(err.error);
        }
      });
    });
  }

  unblockUser(id: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.apiService.unblockUser(id).subscribe( response => {
        if (response) (response.message === 'OK') ? resolve(true) : resolve(false);
      },
      err => {
        if(err) reject(err.error);
      });
    });
  }

  updateUser(user: User): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.apiService.updateUser(user).subscribe( response => {
        if (response) {
          (response.message === 'OK') ? resolve(true) : resolve(false);
        }
      },
      err => {
        if (err) {
          reject(err.error);
        }
      });
    });
  }

  getUserDetails(id: string): Promise<User> {
    return new Promise((resolve, reject) => {
      this.apiService.getUserDetails(id).subscribe( response => {
        if (response) {
          resolve(response.data);
        }
      },
      err => {
        if (err) {
          reject(err);
        }
      });
    });
  }

  getUserList(): Promise<User[]> {
    return new Promise((resolve, reject) => {
      this.apiService.getUsersList().subscribe( 
        response => {
          if (response) {
            resolve(response.data);
          }
      },
      err => {
        if (err) {
          reject(err);
        }
      });
    });
  }

  constructor(private apiService: ApiService) { }

}
