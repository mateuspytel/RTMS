import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../../../model/Users';
import { Router } from '@angular/router';
import { PopupService } from '../../popup/popup.service';
import { PopupType, Popup } from '../../../model/Popup';
import { ApiService } from '../../../apiservice/api.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.css']
})
export class UsersListComponent implements OnInit {

  _users: User[];

  _role: string;

  blockUser(id: string): void {
    const popup: Popup = this.popupService.showDecisionPopup(PopupType.BLOCK, 'Zablokować?');
    popup.getReaction().subscribe( react => {
      if (react === true) {
        this.userService.blockUser(id).then( result => {
          if (result === true) {
            this.popupService.clearPopup();
            this.userService.getUserList().then( users => {
              this._users = users;
            });
          }else {
            // Show error popup
          }
        })
      } else {
        this.popupService.clearPopup();
      }
    });
  }

  unblockUser(id: string): void {
    this.popupService.showDecisionPopup(PopupType.WARNING, 'Odblokować użytkownika?').getReaction().subscribe( reaction => {
      if (reaction === true) {
        this.userService.unblockUser(id).then( result => {
          if (result === true) {
            this.popupService.clearPopup();
            this.userService.getUserList().then( users => {
              this._users = users;
            });
          }
        })
      }else {
        this.popupService.clearPopup();
      }
    });
  }

  editUser(id: string): void {
    this.router.navigateByUrl('/app/users-list/manage/edit?_id=' + id);
  }

  constructor(private userService: UserService, private router: Router, private popupService: PopupService, private apiService: ApiService) { 
    this._role = this.apiService.getUserRole();
    this.userService.getUserList().then( users => {
      this._users = users;
    })
    .catch( err => {
      console.log('Error during fetching user list');
    });
  }

  ngOnInit() {
  }

}
