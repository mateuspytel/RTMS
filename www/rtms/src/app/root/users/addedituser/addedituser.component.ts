import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core';
import { User, USER_ROLES } from '../../../model/Users';
import { UserService } from '../user.service';
import { ViewChildren } from '@angular/core';
import { ValidatedinputComponent } from '../../validatedinput/validatedinput.component';
import { PopupService } from '../../popup/popup.service';
import { PopupType } from '../../../model/Popup';
import { ViewChild } from '@angular/core';
import { ValidatedselectComponent } from '../../validatedselect/validatedselect.component';
import { ApiService } from '../../../apiservice/api.service';

@Component({
  selector: 'app-addedituser',
  templateUrl: './addedituser.component.html',
  styleUrls: ['./addedituser.component.css']
})
export class AddedituserComponent implements OnInit, OnDestroy {

  @ViewChildren(ValidatedinputComponent) inputs: ValidatedinputComponent[];
  @ViewChild(ValidatedselectComponent) select: ValidatedselectComponent;
  _userForm = new User();
  _routeSubscription: Subscription;
  _context: string;
  _rolesOptions = USER_ROLES;

  validateInputs(err): void {
    this.inputs.forEach( input => {
      input.validate(err);
    });
    this.select.validate(err)
  }

  clearErrors(): void {
    this.inputs.forEach( input => { input.error = ''; });
  }

  submitUser(): void {
    switch(this._context) {
      case 'add' : {
        this.userService.createUser(this._userForm).then( res => {
          if (res === true) {
            this.popupService.showPopup(PopupType.OK, 'ZAPISANO');
          }
        })
        .catch( err => {
          this.clearErrors();
          this.validateInputs(err);
        })
        break;
      }
      case 'edit' : {
        this.userService.updateUser(this._userForm).then( res => {
          if (res === true) {
            this.popupService.showPopup(PopupType.OK, 'ZAPISANO');
          }
        })
        .catch( err => {
          this.validateInputs(err);
        });
        break;
      }
    }
  }

  constructor(private activeRoute: ActivatedRoute, private userService: UserService, private popupService: PopupService) {
    this._routeSubscription = this.activeRoute.params.subscribe( params => {
      const context = params['context'];
      if (context === 'add') {
        this._context = 'add';
      }else if (context === 'edit') {
        this._context = 'edit';
        this.activeRoute.queryParams.subscribe( params => {
          const id = params['_id'];
          if (id) {
            this.userService.getUserDetails(id).then( user => {
              this._userForm = user;
            })
            .catch( err => {
              console.log('Error during fetching user details');
            });
          }
        });
      }
    })
    
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this._routeSubscription.unsubscribe();
  }
}
