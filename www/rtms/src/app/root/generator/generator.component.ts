import { Component, OnInit, ViewChild } from '@angular/core';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { ApiService } from '../../apiservice/api.service';
import { Sensor, SENSORTYPE, SensorData } from '../../model/Sensor';
import { IMyDpOptions, IMyDate, IMyOptions } from 'mydatepicker';

@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.css']
})
export class GeneratorComponent implements OnInit {

  _data = {
    chartType : 'AreaChart',
    dataTable : [],
    options: {
      title : "",
      height: 600,
      hAxis: {
        title: 'Czas',
        textStyle: {
          fontSize: 10
        },
        titleTextStyle: {
          italic: false,
          fontSize: 12
        }
      },
      vAxis: {
        title: '',
        minValue: 0,
        maxValue: 100,
        textStyle: {
          fontSize: 10
        },
        titleTextStyle: {
          italic: false,
          fontSize: 12
        }
      },
      pointSize: 4,
      colors: ['#55ACEF', '#4BCF99', '#FE974B', '#FC4C7A']
    }
  }

  _form = {
    sensor_type: '',
    sensors : [],
    _date : {from: {date: {year: 2018, month: 2, day: 4}}, to: {date: {year: 2018, month: 2, day: 4}}}
  }

  _formOptions = {
    _type : [],
    _typeDisabled : false,
    _sensors : { data: [], disabled : true},
    min_timestamp: 0,
    max_timestamp: 0
  }

  _datepickerOptions: IMyOptions = {
    dateFormat: 'dd.mm.yyyy'
  };

  _focus = false;

  _error = '';

  _chartReady = false;

  _sensorList : Sensor[];

  @ViewChild('chart') cchart;

  // Calling this function set disableUntil value until yesterday
  disableUntil(timestamp) {
    let d = this.getMyDateFromTimestamp(timestamp);
    let copy = this.getCopyOfOptions();
    copy.disableUntil = d;
    this._datepickerOptions = copy;
  }

  // Calling this function set disableUntil value until yesterday
  disableSince(timestamp) {
    let d = this.getMyDateFromTimestamp(timestamp);
    let copy = this.getCopyOfOptions();
    copy.disableSince = d;
    this._datepickerOptions = copy;
  }

  // Returns copy of myOptions
  getCopyOfOptions(): IMyOptions {
      return JSON.parse(JSON.stringify(this._datepickerOptions));
  }

  addSensor(sensor_id: string) : void {
    if (!this._form.sensors.find( s => s._id === sensor_id)){
      this._formOptions._typeDisabled = true;
      this._form.sensors.push(this.getSensor(sensor_id));
      this.calculateDates();
    } 
  }

  removeSensor(sensor_id: string): void {
    console.log('Remove sensor: ' + sensor_id);
    this._form.sensors = this._form.sensors.filter( sensor => sensor._id !== sensor_id);
    if (this._form.sensors.length === 0) this._formOptions._typeDisabled = false;
  }

  scrollToData(): void {
    this._focus = true;
    this.scroll.scrollTo({target: 'chart-data'});
  }

  filterSensors(type): void {
    this._formOptions._sensors.data = [];
    this.getSensorsOfType(type).then( sensors => {
      sensors.forEach( s => { 
        this._formOptions._sensors.data.push({value: s._id, label: s.name});
      });
    });
    this._formOptions._sensors.disabled = false;
  }

  getSensorsOfType(type: string): Promise<Sensor[]> {
    return new Promise((resolve, reject) => {
      this.apiService.getSensorsList().then( sensors => {
        this._sensorList = sensors.data;
        resolve(sensors.data.filter( sensor => (sensor.type === type)));
      });
    })
  }

  /*
  * Convert UNIX timestamp to IMyDate object for calendar component
  */
  private getMyDateFromTimestamp(timestamp: number): IMyDate {
    const time = new Date(timestamp);
    return {year: time.getFullYear(), month: time.getMonth() + 1, day: time.getDate()};
  }

  /*
  * Convert IMyDate object to timestamp
  */
  
  private IMyDateToTimestamp(date: IMyDate): number {
    return new Date(date.year, date.month - 1, date.day).getTime();
  }

  dateChanged() {
    console.log('Date from: ' + JSON.stringify(this._form._date.from));
    console.log('Date to: ' + JSON.stringify(this._form._date.to));
    // this.calculateDates();
  }

  calculateDates(): void {
    const active_since_arr = [];
    const lastmodified_arr = [];
    this._error = '';

    // Get date ranges from all selected sensors
    this._form.sensors.forEach( (sensor : Sensor) => {
      active_since_arr.push(sensor.active_since);
      lastmodified_arr.push(sensor.lastmodified);
    });
    // Calculate minimum and maximum common date for all selected sensors
    const min_timestamp = Math.max(...active_since_arr);
    const max_timestamp = Math.max(...lastmodified_arr);
    if (max_timestamp < min_timestamp) {
      this._error = 'Błąd. Dane czujników nie mogą zostać porównane ze względu na brak danych we wskazanym zakresie dat';
    }

    this._form.sensors.forEach( (sensor: Sensor) => {
      active_since_arr.forEach( active_since => {
        if (sensor.lastmodified < active_since) {
          this._error = 'Błąd. Dane czujników nie mogą zostać porównane ze względu na brak danych we wskazanym zakresie dat';
        }
      });
      lastmodified_arr.forEach( lastmodified => {
        if (sensor.active_since > lastmodified) {
          this._error = 'Błąd. Dane czujników nie mogą zostać porównane ze względu na brak danych we wskazanym zakresie dat';
        }
      })
    });

    // Lock calendar range
    const min_date = this.getMyDateFromTimestamp(min_timestamp);
    const max_date = this.getMyDateFromTimestamp(max_timestamp);
    this._formOptions.min_timestamp = min_timestamp;
    this._formOptions.max_timestamp = max_timestamp;
    this._form._date.from = {date: min_date};
    this._form._date.to = {date: max_date};
    this.disableUntil(min_timestamp);
    this.disableSince(max_timestamp);
  }

  getSensor(_id: string) : Sensor {
    return this._sensorList.find(sensor => sensor._id === _id);
  }

  generateChart() {
    var ids = [];
    this._form.sensors.forEach( s => { ids.push(s._id); });
    this.apiService.getChartData(ids, this.IMyDateToTimestamp(this._form._date.from.date), this.IMyDateToTimestamp(this._form._date.to.date))
    .then( chart_data => {
      if (this._data.dataTable.length === 0) {
        this._data.dataTable = chart_data.data;
        let chart_title = 'Wykres';
        chart_data.data[0].forEach( (name, index) => {
          if (index > 0) chart_title += ' ' + name + '/';
        });
        this._data.options.title = chart_title;
      } else {
        let chart_title = 'Wykres';
        chart_data.data[0].forEach( (name, index) => {
          if (index > 0) chart_title += ' ' + name + '/';
        });
        this._data.options.title = chart_title;
        this._data.dataTable = chart_data.data;
        this.cchart.wrapper.setDataTable(chart_data.data);    
        this.cchart.redraw();
      }
      this._chartReady = true;
      this.scroll.scrollTo({target: 'chart'});
    });
  }

  constructor(private scroll: ScrollToService, private apiService: ApiService) { 
    Object.entries(SENSORTYPE).forEach(([key, value]) => {
      this._formOptions._type.push({value: value.type, label: value.text});
    });
  }

  ngOnInit() {
  }

}
