import { Component, OnInit } from '@angular/core';
import { Sensor, AlertData, SENSORTYPE } from '../../model/Sensor';
import { ViewChildren } from '@angular/core';
import { ValidatedinputComponent } from '../validatedinput/validatedinput.component';
import { ValidatedselectComponent } from '../validatedselect/validatedselect.component';
import { ApiService } from '../../apiservice/api.service';
import { ActivatedRoute } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';
import { PopupService } from '../popup/popup.service';
import { PopupType } from '../../model/Popup';
import { Router } from '@angular/router';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

@Component({
  selector: 'app-addeditsensor',
  templateUrl: './addeditsensor.component.html',
  styleUrls: ['./addeditsensor.component.css']
})
export class AddeditsensorComponent implements OnInit, OnDestroy {


  @ViewChildren(ValidatedinputComponent) inputs: ValidatedinputComponent[];
  @ViewChildren(ValidatedselectComponent) selects: ValidatedselectComponent[];

  _sensorForm = new Sensor();

  _alert = {
    type: '',
    name: '',
    threshold: 0,
    receivers_list: []
  }

  paramSub: Subscription;
  _context: string;
  _navigationbarTitle: string;

  _sensorType = [];

  _alertOptions =  [];

  _role: string;

  validateInputs(err: any) {
    this.inputs.forEach( input => { input.validate(err); });
    this.selects.forEach( select => { select.validate(err); });
  }

  validateAlertInputs(): boolean {
    let err = false;
    const input_names = ['alert_name', 'alert_threshold'];
    this.inputs.forEach( input => {
      const filtered = input_names.find( input_name => (input_name === input.name));
      if (filtered) {
        if (input.value.length === 0 || input.value === 0) {
          input.error = "Pole jest wymagane";
          err = true;
        }
      }
    });

    const input_select = 'alert_type';
    this.selects.forEach( selects => {
      if (selects.name === input_select) {
        if (!selects.value) {
          selects.error = "Pole jest wymagane";
          err = true;
        }
      }
    });

    if (this._alert.receivers_list.length === 0) {
      this.selects.forEach( select => {
        if (select.name === 'alert_receivers') select.error = "Odbiorcy są wymagani";
      });
    }

    return err;
  }

  addAlert(): void {
    var alert = new AlertData();
    alert.name = this._alert.name;
    alert.alert_type = this._alert.type;
    alert.threshold = this._alert.threshold;
    alert.receivers_list = this._alert.receivers_list;
    const err = this.validateAlertInputs();
    if (!err && this._sensorForm._id) {
      if (!this._sensorForm.alerts) this._sensorForm.alerts = [];
      this._sensorForm.alerts.push(alert);
      this.updateSensorDetails();
      this._alert = { type: '', name: '', threshold: 0, receivers_list: [] };
    }else {
      this._sensorForm.alerts = [];
      this._sensorForm.alerts.push(alert);
      this.submitSensor();
    }

  }

  submitSensor() {
    switch(this._context) {
      case 'add' : {
        this.apiservice.registerSensor(this._sensorForm).then(res => {
          this.popupService.showPopup(PopupType.OK, 'ZAPISANO');
        }).catch(
          err => {
            this.scroll.scrollTo({target: 'dest'});
            this.validateInputs(err);
          }
        );
        break;
      }
      case 'edit' : {
        this.apiservice.updateSensorDetails(this._sensorForm).then(res => {
          this.popupService.showPopup(PopupType.OK, 'ZAPISANO');
        })
        .catch( err => {
          this.validateInputs(err);
        });
        break;
      }
    }
  }

  removeSensor() {
    this.popupService.showDecisionPopup(PopupType.WARNING, 'Usunąć czujnik?').getReaction().subscribe( reaction => {
      if (reaction === true) { this.apiservice.removeSensor(this._sensorForm._id).then(() => { 
        this.popupService.clearPopup();
        this.router.navigateByUrl('/app/sensors-list');
       }); }
      else { this.popupService.clearPopup(); }
    });
  }

  updateSensorDetails() {
    this.apiservice.updateSensorDetails(this._sensorForm)
    .then(res => { 
      this._sensorForm = res.data;
      console.log('Updated!: ' + JSON.stringify(res.data));
    })
    .catch(err => {console.log('Error during update!')});
  }

  deleteAlert(id: string) {
    this.apiservice.deleteAlert(this._sensorForm._id, id).then(res => { 
      for(let i=0; i<this._sensorForm.alerts.length; i++){
        if (this._sensorForm.alerts[i]._id === id) {
          this._sensorForm.alerts.splice(i, 1);
          break;
        }
      }
    }).catch(err => {console.log('Error: ' + err);})
  }

  prepareSensorForm(id: string): void {
    this.apiservice.getSensorsDetails(id).then( sensor => {
      this._sensorForm = sensor.data;
    })
  }

  addReceiver(receiver) {
    console.log('Adding: ' + receiver);
    this._alert.receivers_list.push(receiver);
  }

  removeReceiver(alert_arr: any[], alert_index: number, receiver_index: number) {
    alert_arr[alert_index].receivers_list.splice(receiver_index, 1);
    if (this._sensorForm._id) this.updateSensorDetails();
  }

  constructor(private apiservice: ApiService, private activatedRoute: ActivatedRoute, private popupService: PopupService,
              private router: Router, private scroll: ScrollToService) {
    this._role = this.apiservice.getUserRole();
    this.paramSub = this.activatedRoute.params.subscribe( params => {
      if (params['context'] === 'add'){
        this._context = 'add';
        this._navigationbarTitle = 'Dodaj czujnik';
        this._sensorForm.active = false;
      }else if (params['context'] === 'edit') {
        this.activatedRoute.queryParams.subscribe( params => {
          this._navigationbarTitle = 'Edytuj czujnik';
          this._context = 'edit';
          const id = params['_id'];
          this.prepareSensorForm(id);
        });
      }
      this.apiservice.getUsersList().subscribe( users => {
        users.data.forEach( usr => this._alertOptions.push({value: usr.email, label: usr.email}));
      });
    });
    
    Object.entries(SENSORTYPE).forEach(([key, value]) => {
      this._sensorType.push({value: value.type, label: value.text});
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.paramSub.unsubscribe();
  }
}
