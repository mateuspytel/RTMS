import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditsensorComponent } from './addeditsensor.component';

describe('AddeditsensorComponent', () => {
  let component: AddeditsensorComponent;
  let fixture: ComponentFixture<AddeditsensorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditsensorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditsensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
