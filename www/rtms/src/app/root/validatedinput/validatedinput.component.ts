import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@angular/forms/src/directives/control_value_accessor';
import { ElementRef } from '@angular/core';

const noop = () => {
};

export const TEST_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => ValidatedinputComponent),
  multi: true
};

@Component({
  selector: 'validated-input',
  templateUrl: './validatedinput.component.html',
  styleUrls: ['./validatedinput.component.css'],
  providers: [TEST_VALUE_ACCESSOR]
})
export class ValidatedinputComponent implements ControlValueAccessor {

  @Input() placeholder: string;

  private innerValue: string = '';

  @Input() error: string;

  @Input() name: string;

  type: string;

  private onChangeCallback: (_ : any ) => void  = noop;
  private onTouchedCallback: () => void = noop;

  get value(): any {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  onBlur() {
    this.onTouchedCallback();
  }

  onFocus() {
    this.error = undefined;
  }

  writeValue(obj: any): void {
    this.innerValue = obj;
  }
  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  validate(obj: any) {
    Object.entries(obj).forEach(([key, value]) => {
      if (this.name === key) {
        this.error = value;
      }
    }); 
  }

  constructor(private el: ElementRef) { this.type = this.el.nativeElement.getAttribute('type'); }

}
