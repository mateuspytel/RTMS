import { Component, OnInit } from '@angular/core';
import { PopupService } from '../popup/popup.service';
import { PopupType } from '../../model/Popup';
import { ApiService } from '../../apiservice/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {

  _selected;

  _role: string;

  _user = {
    firstname : '',
    lastname : ''}

  select(index): void {
    this._selected = index;
  }

  logout() {
    this.apiService.logout();
  }

  constructor(private apiService: ApiService) { this._role = apiService.getUserRole(); var user = this.apiService.getUserData();
     this._user.firstname = user.firstname; this._user.lastname = user.lastname; }

  ngOnInit() {
  }

}
