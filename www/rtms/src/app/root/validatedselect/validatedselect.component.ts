import { Component, OnInit, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@angular/forms/src/directives/control_value_accessor';
import { ElementRef } from '@angular/core';

const noop = () => {
};

export const TEST_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => ValidatedselectComponent),
  multi: true
};

@Component({
  selector: 'validated-select',
  templateUrl: './validatedselect.component.html',
  styleUrls: ['./validatedselect.component.css'],
  providers: [TEST_VALUE_ACCESSOR]
})
export class ValidatedselectComponent implements ControlValueAccessor {

  private onChangeCallback: (_ : any ) => void  = noop;
  private onTouchedCallback: () => void = noop;
  private innerValue: any;
  error: string;
  
  @Input() name: string;
  @Input() options: any;
  @Input() placeholder : string;
  @Input() disabled : boolean;
  @Output() selected = new EventEmitter<string>();

  get value(): any {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.selected.emit(this.innerValue);
      this.onChangeCallback(v);
    }
  }

  onFocus() {
    this.error = undefined;
  }

  writeValue(obj: any): void {
    this.innerValue = obj;
  }
  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  validate(obj: any) {
    Object.entries(obj).forEach(([key, value]) => {
      if (this.name === key) {
        this.error = value;
      }
    }); 
  }

  constructor(private el: ElementRef) { 
  }

  
}
