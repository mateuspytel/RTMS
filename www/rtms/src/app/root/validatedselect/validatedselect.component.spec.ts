import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidatedselectComponent } from './validatedselect.component';

describe('ValidatedselectComponent', () => {
  let component: ValidatedselectComponent;
  let fixture: ComponentFixture<ValidatedselectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidatedselectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidatedselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
