import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RootComponent } from './root/root.component';
import { SensorComponent } from './sensor/sensor.component';
import { SensorchartComponent } from './charts/sensorchart/sensorchart.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts/google-charts.module';
import { PanelComponent } from './panel/panel.component';
import { AddeditsensorComponent } from './addeditsensor/addeditsensor.component';
import { ValidatedinputComponent } from './validatedinput/validatedinput.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidatedselectComponent } from './validatedselect/validatedselect.component';
import { TimestamptodatePipe } from '../pipes/timestamptodate.pipe';
import { MyDatePickerModule } from 'mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { NouisliderModule } from 'ng2-nouislider';
import { SearchComponent } from './search/search.component';
import { SearchService } from './search/search.service';
import { UsersListComponent } from './users/users/userslist.component';
import { UserService } from './users/user.service';
import { AddedituserComponent } from './users/addedituser/addedituser.component';
import { PopupComponent } from './popup/popup.component';
import { PopupService } from './popup/popup.service';
import { LocalStorageService } from 'ngx-webstorage';
import { StorageService } from '../storageservice/storage.service';
import { WebsocketService } from '../websocket/websocket.service';
import { ContenteditableDirective } from '../directives/contenteditable.directive';
import { SensortypenamePipe } from '../pipes/sensortypename.pipe';
import { UserrolePipe } from '../pipes/userrole.pipe';
import { AlertlogsComponent } from './alertlogs/alertlogs.component';
import { BooleanformatPipe } from '../pipes/booleanformat.pipe';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { GeneratorComponent } from './generator/generator.component';
import { SensoridtonamePipe } from '../pipes/sensoridtoname.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
    Ng2GoogleChartsModule,
    FormsModule,
    ReactiveFormsModule,
    MyDatePickerModule,
    DateTimePickerModule,
    NouisliderModule,
    ScrollToModule.forRoot()
  ],
  declarations: [
    RootComponent,
    SensorComponent,
    SensorchartComponent,
    PanelComponent, 
    AddeditsensorComponent, 
    ValidatedinputComponent, 
    ValidatedselectComponent,
    TimestamptodatePipe,
    SensortypenamePipe,
    BooleanformatPipe,
    SensoridtonamePipe,
    UserrolePipe,
    SearchComponent,
    UsersListComponent,
    AddedituserComponent,
    PopupComponent,
    ContenteditableDirective,
    AlertlogsComponent,
    GeneratorComponent],
  providers: [
      SearchService,
      UserService,
      PopupService,
      LocalStorageService,
      StorageService,
      WebsocketService
  ],
  exports: [ValidatedinputComponent]
})
export class RootModule { }
