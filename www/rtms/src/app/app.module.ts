import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ApproutingRoutingModule } from './approuting/approuting-routing.module';
import { RouterModule } from '@angular/router';
import { AuthorizationModule } from './authorization/authorization.module';
import { RootModule } from './root/root.module';
import { ApiService } from './apiservice/api.service';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthorizationInterceptor } from './apiservice/httpinterceptor';
import { Ng2Webstorage, LocalStorageService } from 'ngx-webstorage';
import { SocketIoConfig } from 'ng-socket-io';
import { SocketIoModule } from 'ng-socket-io/dist/src/socket-io.module';
import { HTTPCONFIG } from './apiservice/httpconfig';
import { SensortypenamePipe } from './pipes/sensortypename.pipe';
import { UserrolePipe } from './pipes/userrole.pipe';
import { BooleanformatPipe } from './pipes/booleanformat.pipe';
import { SensoridtonamePipe } from './pipes/sensoridtoname.pipe';

const socket_config: SocketIoConfig = { url: HTTPCONFIG.socketURL, options: {}};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AuthorizationModule,
    ApproutingRoutingModule,
    Ng2GoogleChartsModule,
    RootModule,
    HttpClientModule,
    Ng2GoogleChartsModule,
    Ng2Webstorage,
    SocketIoModule.forRoot(socket_config)
  ],
  providers: [
    ApiService, 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    },
    LocalStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
