import { Directive, Output, EventEmitter, Input } from '@angular/core';
import { HostListener } from '@angular/core';
import { ElementRef } from '@angular/core';
import { OnInit } from '@angular/core';

@Directive({
  selector: '[contenteditabledirective]'
})
export class ContenteditableDirective implements OnInit {
  
  @Input() contenteditabledirective: string;

  @Output() onEnterEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @HostListener('keydown', ['$event']) keyDownEvent(event: KeyboardEvent) {
    if(event.keyCode == 13) {
      console.log('Enter event!');
      this.onEnterEvent.emit(true);
      event.preventDefault();
    }
  }

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
    this.el.nativeElement.innerText = this.contenteditabledirective;
  }
}
