import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Socket } from 'ng-socket-io';
import 'rxjs/add/operator/map';
import { HTTPCONFIG } from '../apiservice/httpconfig';

@Injectable()
export class WebsocketService {

  public subscribeToSensorsLiveData(_id: string): Observable<any> {
    console.log('Listening from remote server...');
    return this.socket.fromEvent(_id);
  }

  public subscribeToAlertData(): Observable<any> {
    return this.socket.fromEvent('alert');
  }

  constructor(private socket: Socket) {
  }

}
