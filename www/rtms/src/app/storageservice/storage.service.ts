import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Sensor } from '../model/Sensor';

@Injectable()
export class StorageService {

  saveSensorsDetails(sensor: Sensor): void {
    this.localstorage.store('LAST_SENSOR', sensor);
  }

  retrieveLastSavedSensor(): Sensor {
    return this.localstorage.retrieve('LAST_SENSOR');
  }

  constructor(private localstorage: LocalStorageService) { }

}
