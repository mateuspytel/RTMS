import { Injectable } from "@angular/core";
import { HttpInterceptor } from "@angular/common/http/src/interceptor";
import { HttpRequest } from "@angular/common/http/src/request";
import { HttpHandler } from "@angular/common/http/src/backend";
import { Observable } from "rxjs/Observable";
import { HttpEvent, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { HTTPCONFIG } from "./httpconfig";
import 'rxjs/add/operator/do';
import { SessionStorageService } from "ngx-webstorage";
import { Router } from "@angular/router";
import { ApiService } from "./api.service";
import { Injector } from "@angular/core";



@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

    constructor(private sessionStorage: SessionStorageService, private router: Router, private inj: Injector) {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.sessionStorage.retrieve('APIKEY')) {
            request = request.clone({
                setHeaders: {
                    'Authorization' : this.sessionStorage.retrieve('APIKEY'),
                }
            });
        }

        return next.handle(request).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {

            }
        },  (err: any) => {
            if (err instanceof HttpErrorResponse) {
                switch(err.status) {
                    case 400: {break;}
                    case 401: {
                        // this.router.navigateByUrl('/login');
                        if (this.sessionStorage.retrieve('APIKEY')) { this.inj.get(ApiService).logout(); }
                        else { this.router.navigateByUrl('/login');}
                        break;
                    }
                    case 403: {
                        this.router.navigateByUrl('/app/panel');
                    }
                    case 500: {
                        console.log('Server error - status 500');
                        break;
                    }
                }
            }
        });
    }
}