import { Injectable } from '@angular/core';
import { SensorResponse, Sensor, SensorData, SensorDataResponse, SensorChartResponse } from '../model/Sensor';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HTTPCONFIG } from './httpconfig';
import { HttpHeaders } from '@angular/common/http';
import { error } from 'selenium-webdriver';
import { UserResponse, User, UserDetailsResponse, UserLoginResponse } from '../model/Users';
import { AlertLogResponse } from '../model/AlertLog';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Injectable()
export class ApiService {

getUserRole(): string {
  return this.sessionStorage.retrieve('user_details').role;
}

getUserData(): any {
  return this.sessionStorage.retrieve('user_details');
}

/*
* Login user
*/
login(user: any): Promise<UserLoginResponse> {
  return new Promise((resolve, reject) => {
    this.httpClient.post<UserLoginResponse>(HTTPCONFIG.remoteURL + '/login', user).subscribe( response => {
      if (response) {
        this.sessionStorage.store('APIKEY', response.user.apikey);
        this.sessionStorage.store('user_details', {_id: response.user._id, firstname: response.user.firstname,
                                                   lastname: response.user.lastname, role: response.user.role});
        this.getSensorsList().then( res => {
          let sensors = [];
          res.data.forEach(s => {
            sensors.push({_id: s._id, name: s.name});
          })
          this.sessionStorage.store('sensors', sensors);
        });
        this.router.navigateByUrl('/app/panel');
      }
    },
    err => {
      if (err) reject(err.error);
    });
  });
}

/*
* Logs out
*/
logout(): void {
  var key = this.sessionStorage.retrieve('APIKEY');
  this.httpClient.post<any>(HTTPCONFIG.remoteURL + '/logout', {key: key}).subscribe( response => {
    if (response && response.message === 'OK') { 
      this.sessionStorage.clear('APIKEY');
      this.router.navigateByUrl('/login');
    }
  },
  err => {
    if (err) {
      this.sessionStorage.clear('APIKEY');
      this.router.navigateByUrl('/login');
    }
  });
}

/*
* Get user list
*/
getUsersList(): Observable<UserResponse> {
  return this.httpClient.get<UserResponse>(HTTPCONFIG.remoteURL + '/getUsers');
}

/*
* Get user details
*/
getUserDetails(id: string): Observable<UserDetailsResponse> {
  return this.httpClient.get<UserDetailsResponse>(HTTPCONFIG.remoteURL + '/getUserDetails?_id=' + id);
}

/*
* Update user
*/
updateUser(user: User): Observable<UserResponse> {
  return this.httpClient.post<UserResponse>(HTTPCONFIG.remoteURL + '/updateUser', user);
}

/*
* Create user
*/
createUser(user: User): Observable<UserResponse> {
  return this.httpClient.post<UserResponse>(HTTPCONFIG.remoteURL + '/createUser', user);
}

/*
* Block user
*/
blockUser(id: string): Observable<UserResponse> {
  return this.httpClient.post<UserResponse>(HTTPCONFIG.remoteURL + '/blockUser', {_id: id});
}

/*
* Unblock user
*/
unblockUser(id: string): Observable<UserResponse> {
  return this.httpClient.post<UserResponse>(HTTPCONFIG.remoteURL + '/unblockUser', {_id: id});
}

/*
* Get all sensors list from remote API
*/
getSensorsList(): Promise<SensorResponse> {
  return new Promise((resolve, reject) => {
    this.httpClient.get<SensorResponse>(HTTPCONFIG.remoteURL + '/getSensorsList').subscribe(
      response => {
        if (response) {
          resolve(response);
          let sensors = [];
          response.data.forEach(s => {
            sensors.push({_id: s._id, name: s.name});
          })
          this.sessionStorage.store('sensors', sensors);
        }
      },
      err => {
        // Handle error during fetching all sensors
        reject();
      }
    )
  });
}

/*
* Get sensors from remote API
*/
getSensorsDetails(id: string): Promise<SensorDataResponse> {
  console.log('Getting sensor details of id: ' + id);
  let query = '/getSensorsDetails?_id=' + id;
  return new Promise((resolve, reject) => {
    this.httpClient.get<SensorDataResponse>(HTTPCONFIG.remoteURL + query).subscribe(
      response => {
        if (response) {
          resolve(response);
        }
      },
      err => {
        // Handle error during fetching sensor
        reject();
      }
    );
  });
}

/*
* Get sensors data for graph
*/
getSensorsData(id: string): Promise<SensorDataResponse> {
  return new Promise((resolve, reject) => {
    this.httpClient.get<SensorDataResponse>(HTTPCONFIG.remoteURL + '/getSensorsData?_id=' + id).subscribe(
      response => {
        if (response) {
          resolve(response);
        }
      },
      err => {
        if (err) {
          reject(err);
        }
      }
    );
  })
}

/*
* Get sensors data from timerange
*/
getSensorsDataFromRange(id: string, from: number, to: number): Promise<SensorDataResponse> {
  return new Promise((resolve, reject) => {
    this.httpClient.get<SensorDataResponse>(HTTPCONFIG.remoteURL + '/getSensorsDataFromRange?_id=' + id + '&from=' + from + '&to=' + to)
    .subscribe(
      response => {
        if (response) {
          resolve(response);
        }
      },
      err => {
        reject();
      }
    );
  });
}

/*
* Retrieves chart data
*/
getChartData(ids: string[], from: number, to: number) : Promise<SensorChartResponse> {
  return new Promise((resolve, reject) => {
    this.httpClient.post<SensorChartResponse>(HTTPCONFIG.remoteURL + '/getChartData', { _ids: ids, from : from, to : to })
    .subscribe( response => {
      if (response) resolve(response);
    });
  });
}

/*
* Update sensor details
*/
updateSensorDetails(sensor: Sensor): Promise<SensorDataResponse> {
  return new Promise<SensorDataResponse>((resolve, reject) => {
    this.httpClient.post<SensorDataResponse>(HTTPCONFIG.remoteURL + '/updateSensorDetails', sensor).subscribe( 
      response => {
        if (response) {
          resolve(response);
        }
      },
      error => {
        if (error) {
          console.log('Error: ' + JSON.stringify(error));
          reject(error.error);
        }
      }
    )
  });
}

/*
* Register sensor
*/
registerSensor(sensor: Sensor): Promise<string> {
  return new Promise((resolve, reject) => {
    console.log('API SERVICE: ' + JSON.stringify(sensor));
    this.httpClient.post<any>(HTTPCONFIG.remoteURL + '/registerSensor', sensor).subscribe(
      response => {
        if (response) {
          let sensors = [];
          response.data.forEach(s => {
            sensors.push({_id: s._id, name: s.name});
          })
          this.sessionStorage.store('sensors', sensors);
          resolve(response);
        }
      },
      err => {
        reject(err.error);
      }
    );
  });
}

/*
* Removes sensor
*/
removeSensor(sensor_id: string): Promise<boolean> {
  return new Promise((resolve, reject) => {
    this.httpClient.delete(HTTPCONFIG.remoteURL + '/removeSensor?_id=' + sensor_id).subscribe( res => {
      if (res) resolve(true);
    },
    err => {
      reject(err);
    });
  });
}

/*
* Get sensors alerts
*/
getSensorsAlerts(id: string): Promise<SensorDataResponse> {
  return new Promise<SensorDataResponse>((resolve, reject) => {
  this.httpClient.get<SensorDataResponse>(HTTPCONFIG.remoteURL + '/getSensorsAlerts?_id=' + id).subscribe( 
    response => {
      if (response) resolve(response);
    },
    err => {
      reject();
    });
  });
}

getActiveAlerts(id: string): Promise<AlertLogResponse> {
  return new Promise((resolve, reject) => {
    this.httpClient.get<AlertLogResponse>(HTTPCONFIG.remoteURL + '/getActiveAlerts?_id=' + id).subscribe( 
      response => {
        if (response) resolve(response);
      },
      err => {
        if (err) reject(err);
      });
  });
}

/*
* Cancel alert
*/
cancelAlert(id: string): Promise<boolean> {
  return new Promise((resolve, reject) => {
    this.httpClient.post<AlertLogResponse>(HTTPCONFIG.remoteURL + '/cancelAlert', {_id: id}).subscribe(
      response => {
        if (response.message === 'OK') resolve(true);
        else resolve(false);
      },
      err => {
        reject();
      }
    );
  });
}

/*
* Delete alert
*/
deleteAlert(sensor_id: string, id: string): Promise<boolean> {
  return new Promise((resolve, reject) => {
    this.httpClient.delete(HTTPCONFIG.remoteURL + '/deleteAlert?sensor_id=' + sensor_id + '&_id=' + id).subscribe(res => {
      if(res) resolve(true);
    },
    err => { if (err) reject(err.error); });
  });
}

/*
* Retrieves alert logs
*/
getAlertLogs(): Promise<AlertLogResponse> {
  return new Promise((resolve, reject) => {
    this.httpClient.get<AlertLogResponse>(HTTPCONFIG.remoteURL + '/getAlertLogs').subscribe(
      response => {
        if (response) resolve(response);
      },
      err => {
        if (err) reject(err);
      }
    );
  });
}

/*
* Find sensor by phrase
*/
findSensorByPhrase(phrase: string): Observable<SensorResponse> {
  return this.httpClient.get<SensorResponse>(HTTPCONFIG.remoteURL + '/find?phrase=' + phrase);
}

constructor(private httpClient: HttpClient, private sessionStorage: SessionStorageService, private router: Router) { }

}
