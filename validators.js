/*
*   Validate inputs
*/

var config = require('./config.js');

module.exports = {
    /**
     * Validates sensors data during registering new sensor
     * @param {Object} sensor 
     * @returns {Object} errors
     */
    validateRegisterSensor(sensor) {
        return this.validateObject(config.sensor_required_properties, sensor);
    },

    /**
     * Validates creates user route
     */
    validateCreateUser(user) {
        var errors = this.validateObject(config.user_create_required_properties, user);
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(user.email)) errors['email'] = 'Podaj właściwy adres email np. example@mail.pl';
        if (user.password) {
            if (user.password.length < 6) errors["password"] = "Hasło za krótkie. Minimum 6 znaków";
            if (!config.user_roles.hasOwnProperty(user.role)) errors['role'] = 'Podaj właściwą rolę';
        }
        return errors;
    },

    /**
     * Validates update user route
     */
    validateUpdateUser(user) {
        return this.validateObject(config.user_update_required_properties, user);
    },

    validateLogin(user) {
        return this.validateObject(config.user_login_required_properties, user);
    },

    /**
     * 
     * @param {string[]} req_props Required properties string array
     * @param {object} obj Object to validate
     */
    validateObject(req_props, obj) {
        var errors = {};
        req_props.forEach( property => {
            if (!obj.hasOwnProperty(property.field)) {
                errors[property.field] = property.error;
            }else {
                if (obj[property.field].length === 0) {
                    errors[property.field] = property.error;
                }
            }
        });
        return errors;
    }
}