var mailer = require('nodemailer');

var transport = mailer.createTransport(
{
    service: 'gmail',
    auth: {
        user: 'rtms.mailer@gmail.com',
        pass: 'zupazupa123'
    }
});

module.exports = {
    globalOptions: {
        from: 'rtms.mailer@gmail.com'
    },
    sendAlertEmail(sensor_id, sensor_name, sensor_type,
                   localization, alert_threshold, receiver, callback) {
        var unit = (sensor_type === 'temperature') ? ' \u1d52C' : ' %';
        var options = { 
            from: this.globalOptions.from,
            to: receiver,
            subject: 'Przekroczono wartość progową sensora: ' + sensor_name + ' ' + sensor_id,
            text: 'Ustalona wartość alertu ' + alert_threshold + unit + ' została przekroczona dla '
            + 'sensora: ' + sensor_name + ' \nID: ' + sensor_id + ' w lokalizacji: ' 
            + localization + '.'
        }

        transport.sendMail(options, (err, info) => {
            console.log('Sending alert email to: ' + receiver);
            callback(err, info);
        });
    }
}