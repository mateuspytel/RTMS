const express = require('express');
const router = express.Router();
var getIP = require('ipware')().get_ip;
var dao = require('./dao.js');
var validators = require('./validators.js');
var socket = require('./socket.js');
var mailing = require('./mailing.js');
var apikeyvault = require('./apikeyvault.js');
var config = require('./config.js');
var authentication = require('./authentication.js');
var chart = require('./chart.js');
var apikey;

dao.getAuthorizationAPIKey('root', (err, key) => {
    if (key) apikeyvault.addApiKey({key: key.apikey, expirydate: key.expirydate, role: 'ROLE_ROOT'});
});

var logger = function(req, res, next) {
    var auth_msg;
    var authkey = JSON.parse(JSON.stringify(req.headers)).authorization;
    (authkey === apikey) ? auth_msg = 'API key: ' + authkey :
                           auth_msg = 'Not authorized';

    console.log('Route: ' + req.originalUrl, ' accessed by: ' + (getIP(req).clientIp).substr(7)
        + ' at: ' + new Date().toLocaleDateString() 
        + ' ' +  new Date().toLocaleTimeString());
    dao.logActivity((getIP(req).clientIp) ? (getIP(req).clientIp).substr(7) : getIP(req).clientIp
    ,req.originalUrl, auth_msg, (err, log) => {});
    next();
}

var checkAuthorizationKey = function(req, res, next) {
    if (!JSON.parse(JSON.stringify(req.headers)).authorization) res.status(401).send('Not authorized').end();
    apikeyvault.findApiKey(JSON.parse(JSON.stringify(req.headers)).authorization, (key) => {
        var currentTime = new Date();
        if (key && key.expirydate >= currentTime.getTime()) next();
        else res.status(401).send('Not authorized').end();
    });
}

var exclude = function(paths, middleware) {
    return function(req, res, next) {
        (paths.indexOf(req.path) > -1) ? next() : middleware(req, res, next);
    }
}

router.use(logger);
router.use(exclude(['/login', '/logout'], checkAuthorizationKey));

/********************************************************************************************
 * USER CONTROLLER
 ********************************************************************************************/

router.post('/createUser', authentication.adminAuthorization, function(req, res) {
    var user = req.body;
    var errors = validators.validateCreateUser(user);
    if (Object.getOwnPropertyNames(errors).length !== 0) {
        res.status(400).json(errors);
    }else {
        dao.createUser(user, (err, u) => {
            if (err) {
                if (err.hasOwnProperty(config.user_create_unique_fields.field)) {
                    errors[config.user_create_unique_fields.field] = config.user_create_unique_fields.error;
                    res.status(400).json(errors);
                }else {
                    res.status(400).send('Error! Creating user failed').end();
                }  
            }
            if (u) res.status(200).json({message: 'OK'});
        });
    }
});

router.get('/getUsers', authentication.techAuthorization, function(req, res) {
    dao.getUserList((err, users) => {
        if (err || !users) res.status(400).send('Users not found');
        if (users) {
            res.status(200).json({message: 'OK', data: users});
        }
    });
});

router.get('/getUserDetails', authentication.adminAuthorization, function(req, res) {
    var id = req.query._id;
    if (id) {
        dao.getUserDetails(id, (err, user) => {
            if (err || !user) res.status(400).send('Error during fetching user details').end();
            if (user) res.status(200).json({message: 'OK', data: user});
        });
    }else {
        res.status(400).send('Error during fetching user details').end();
    }
});

router.post('/updateUser', authentication.adminAuthorization, function(req, res) {
    var user = req.body;
    var errors = validators.validateUpdateUser(user);
    if (Object.getOwnPropertyNames(errors).length !== 0) {
        res.status(400).json(errors);
    }else {
        dao.updateUser(user, (err, result) => {
            if (err || !result) res.status(400).send('Error updating user faield').end();
            if (result) {
                res.status(200).json({message: 'OK'});
            }
        });
    }
});

router.post('/blockUser', authentication.adminAuthorization, function(req, res) {
    var user_id = req.body._id;
    if (user_id) {
        dao.blockUser(user_id, (err, result) => {
            if (err || !result) res.status(400).send('Error! Blocking user failed').end();
            if (result) {
                apikeyvault.removeApiKey(result.apikey.key);
                res.status(200).json({message: 'OK'});
            }
        });
    }
});

router.post('/unblockUser', authentication.adminAuthorization, function(req, res) {
    var user_id = req.body._id;
    if (user_id) {
        dao.unblockUser(user_id, (err, result) => {
            if (err || !result) res.status(400).send('Error! Unblocking user failed!').end();
            if (result) res.status(200).json({message: 'OK'});
        });
    }
});

router.post('/login', function(req, res) {
    var user = req.body;
    var errors = validators.validateLogin(user);
    if (Object.getOwnPropertyNames(errors).length !== 0) {
        res.status(400).json(errors);
    }else {
        dao.login(user, (err, user) => {
            if (err) {
                errors[err.field] = err.error;
                res.status(400).json(errors);
            }else {
                dao.generateAndSaveAPIKey(user._id, (error, auth_user) => {
                    if (error || !auth_user) res.status(400).send('Login failed during saving key!').end();
                    if (auth_user) {
                        res.status(200).json({message: 'Success', user: auth_user});
                    }
                });
            }
        });
    }
});

router.post('/logout', function(req, res) {
    var key = req.body.key;
    dao.logout(key, (err, result) => {
        if (err || !result) res.status(400).send('Logging out failed!').end();
        if (result) {
            res.status(200).json({message: 'OK'});
            apikeyvault.removeApiKey(key);
        }
    });
});

/*******************************************************************************************
 * SENSOR CONTROLLER
 *******************************************************************************************/

router.post('/registerSensor', authentication.techAuthorization, function(req, res) {
    var sensor = req.body;
    var errors = validators.validateRegisterSensor(sensor);
    if (Object.getOwnPropertyNames(errors).length !== 0) {
        res.status(400).json(errors);
    }else {
        dao.createSensor(sensor, (err, s) => {
            if (err || !s) res.status(400).send('Register sensor error').end();
            if (s) res.status(200).json({message: 'Successfully registered sensor'});
        });
    }
});

router.delete('/removeSensor', authentication.techAuthorization, function(req, res) {
    var id = req.query._id;
    dao.removeSensor(id, (err) => {
       if (err) res.status(400).send('Error! Removing sensor failed').end();
       else res.status(200).json({message: 'OK'}); 
    });
});

router.get('/getSensorsList', authentication.userAuthorization, function(req, res) {
    dao.getSensorsList((err, sensors) => {
        if (err || !sensors) {
            res.status(400).send('Error during fetching sensors list').end();
        }
        if (sensors) {
            res.status(200).json({message: 'OK', data: sensors});
        }
    });
});

router.get('/getSensorsDetails', authentication.userAuthorization, function(req, res) {
    var sensor_id = req.query._id;
    if (sensor_id) {
        dao.getSensorsDetails(sensor_id, (err, sensor) => {
            // Handle error
            if (err || !sensor) res.status(400).send('Sensor not found').end();
            if (sensor) { res.status(200).json({message: 'OK', data: sensor}); }
        });
    }else {
        res.status(400).send('No sensor found').end();
    }
});

router.get('/getSensorsData', authentication.userAuthorization, function(req, res) {
    var sensor_id = req.query._id;
    dao.getSensorsData(sensor_id, (err, sensor) => {
        if (err || !sensor) {
            res.status(400).send('Error during fetching sensors data').end();
        }
        if (sensor) {
            res.status(200).json({message: 'OK', data: sensor});
        }
    });
});

router.get('/getSensorsDataFromRange', authentication.userAuthorization, function(req, res) {
    var sensor_id = req.query._id;
    var timestamp_from = req.query.from;
    var timestamp_to = req.query.to;
    dao.getSensorsDataFromRange(sensor_id, timestamp_from, timestamp_to, (err, sensor) => {
        if (err || !sensor) {
            res.status(400).send('Error sensor\'s data not found').end();
        }
        if (sensor) {
            res.status(200).json({message: 'OK', data: sensor});
        }
    });
});

router.get('/getSensorsAlerts', authentication.userAuthorization, function(req, res) {
    var sensor_id = req.query._id;
    dao.getSensorsAlerts(sensor_id, (err, alert) => {
        if (err || !alert) {
            res.status(400).send('Error during fetching alerts').end();
        }     
        if (alert) {
            res.status(200).json({message: 'OK', data: alert})
        }
    });
})

router.post('/updateSensorDetails', authentication.techAuthorization, function(req, res) {
    var sensor = req.body;
    var errors = validators.validateRegisterSensor(sensor);
    if (Object.getOwnPropertyNames(errors).length !== 0) {
        res.status(400).json(errors);
    }else {
        dao.updateSensorsDetails(sensor,
            (err, result) => {
                if (err || !result) {
                    res.status(400).send('Error! Updating sensors details failed').end();
                }
                if (result) {
                    res.status(200).json({message: 'OK', data: result});
                }
        });
    }
});

router.post('/updateSensorsData', authentication.techAuthorization, function(req, res) {
    var sensor_id = req.body._id;
    var value = req.body.value;

    if (sensor_id && value) {
        // Catch sensor errors
        dao.updateSensorsData(sensor_id, value, (err, sensor) => {
            if (err || !sensor) res.status(400).send('Sensor not active or corrupted').end();
            if (sensor) {
                logAndEmitAlert(sensor, value);
                socket.broadcastSensorData(sensor._id, sensor.lastmodified, value);
				res.status(200).json({message: 'OK'});
            }
        });
    }else {
        res.status(400).send('Error! Updating sensor data failed!').end();
    }
});

router.post('/updateAnonymousSensorData', function(req, res) {
    var serial_number = req.body.sn;
    var value = req.body.value;
    var sensor_id = req.body._id;
    if (!sensor_id && serial_number) {
        // Update anonymously 
        dao.updateAnonymouslySensorsData(serial_number, value, (err, sensor) => {
            if (err || !sensor) res.status(400).json({message: "Error. Requested sensor is not active"});
            else {
                logAndEmitAlert(sensor, value);
                socket.broadcastSensorData(sensor._id, sensor.lastmodified, value);
				res.status(200).json({message: 'OK'});
            }
        });
    }
    if (sensor_id && !serial_number) {
        dao.updateSensorsData(sensor_id, value, (err, sensor) => {
            if (err || !sensor) res.status(400).json({ message: "Error. Requested sensor is not active" });
            else {
                logAndEmitAlert(sensor, value);
                socket.broadcastSensorData(sensor._id, sensor.lastmodified, value);
				res.status(200).json({message: 'OK'});
            }
        });
    }

    if (sensor_id && serial_number) {
        dao.updateSensorsData(sensor_id, value, (err, sensor) => {
            if (err || !sensor) res.status(400).json({ message: "Error. Requested sensor is not active" });
            else {
                logAndEmitAlert(sensor, value);
                socket.broadcastSensorData(sensor._id, sensor.lastmodified, value);
				res.status(200).json({message: 'OK'});
            }
        });
    }
});

/********************************************************************************************
 * ALERTS CONTROLLER
 ********************************************************************************************/
router.get('/getAlertLogs', authentication.userAuthorization, function(req, res) {
    dao.getAlertLogs((err, alert_logs) => {
        if (err || !alert_logs) res.status(400).send('Error! Alert logs not found');
        if (alert_logs) res.status(200).json({message: 'OK', data: alert_logs});
    });
});


router.get('/getActiveAlerts', authentication.userAuthorization, function(req, res) {
    var id = req.query._id;
    dao.getActiveAlerts(id, (err, alerts) => {
        if (err || !alerts) res.status(400).send('Error! No active alerts found!').end();
        if (alerts) res.status(200).json({message: 'OK', data: alerts});
    });
});

router.post('/updateAlert', authentication.userAuthorization, function(req, res) {
    var alerts = req.body;
    dao.updateAlert(alerts, (err, a) => {
        if (err || !a) {
            res.status(400).end();
        }
        if (a) {
            res.status(200).json({message: 'OK'});
        }
    });

});

router.post('/cancelAlert', authentication.userAuthorization, function(req, res) {
    var id = req.body._id;
    dao.cancelAlert(id, (err, alertlog) => {
        if (err || !alertlog) res.status(400).send('Error! tCancelling error failed!').end();
        if (alertlog) {
            dao.updateSensorsAlertState(alertlog.sensor_id, alertlog.alert_name, true);
            res.status(200).json({message: 'OK'});
        }
    });
});

router.delete('/deleteAlert', authentication.techAuthorization, function(req, res) {
    var sensor_id = req.query.sensor_id;
    var id = req.query._id;
    dao.deleteAlert(sensor_id, id, (err, result) => {
        if (err) res.status(400).send('Deleting alert failed').end();
        if (result) res.status(200).json({message: 'OK'});
    });
});

/********************************************************************************************
 * MISC
 *******************************************************************************************/

router.get('/find', function(req, res) {
    var phrase = req.query.phrase;
    if (!phrase) return;
    dao.findSensor(phrase, (err, sensor) => {
        if (err || !sensor) res.status(400).send('Sensor not found').end();
        if (sensor) {
            res.status(200).json({message: 'OK', data: sensor});
        }
    });
});

router.post('/getChartData', function(req, res) {
    var sensors = req.body._ids;
    var from = req.body.from;
    var to = req.body.to;
    chart.getChartData(sensors, from, to).then( data => {
        if (data) res.status(200).json({ message: 'OK', data: data });
    });
});

var logAndEmitAlert = function(sensor, value) {
    // Check if sensor has defined alerts
    if (sensor.alerts) {
        if (sensor.alerts.length > 0) {
            // Check if value exceeds sensor's alert threshold
            function findActiveAlert(element) {
                if (element.active === true && value > element.threshold) return element;
                else return undefined;
            }
            var alerts_to_trigger = sensor.alerts.find(findActiveAlert);
            if (alerts_to_trigger) {
                // Save alert log to database
                dao.logAlert(
                sensor._id, 
                sensor.type,
                sensor.localization,
                alerts_to_trigger.name,
                alerts_to_trigger.alert_type, 
                alerts_to_trigger.threshold,
                alerts_to_trigger.receivers_list,
                (err, alert_log) => {
                    // Emit alert notification
                    socket.broadcastAlert(alert_log);
                    // Change sensor's flag to inactive
                    dao.updateSensorsAlertState(sensor._id, alerts_to_trigger.name, false);
                    // Send alert email
                    if (alerts_to_trigger.receivers_list && alerts_to_trigger.receivers_list.length !== 0) {
                        alerts_to_trigger.receivers_list.forEach( receiver => {
                            if (alerts_to_trigger.alert_type === config.alert_type.MAIL) {
                                console.log('Sending email to: ' + receiver);
                                mailing.sendAlertEmail(sensor._id, sensor.name, sensor.type,
                                sensor.localization, alerts_to_trigger.threshold, receiver);
                            }
                        })
                    }
                });
            }
        }
    }
}

module.exports = router;