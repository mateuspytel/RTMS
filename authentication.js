/******************************************************************************************************************
 * Authentication middleware
 ******************************************************************************************************************/
var config = require('./config.js');
var apivault = require('./apikeyvault.js');
module.exports = {
    /**
     * Root role authorization
     */
    rootAuthorization(req, res, next) {
        var apikey = JSON.parse(JSON.stringify(req.headers)).authorization;
        var role = apivault.findApiKey(apikey, (key) => {
            (key.role === config.user_roles.ROLE_ROOT) ? next() : res.status(403).send('Forbidden').end(); 
        });
    },

    /**
     * Admin role authorization
     */
    adminAuthorization(req, res, next) {
        var apikey = JSON.parse(JSON.stringify(req.headers)).authorization;
        var role = apivault.findApiKey(apikey, (key) => {
            (key.role === config.user_roles.ROLE_ROOT ||
             key.role === config.user_roles.ROLE_ADMIN) ? next() : res.status(403).send('Forbidden').end(); 
        });
    },

    /**
     * Tech role authorization
     */
    techAuthorization(req, res, next) {
        var apikey = JSON.parse(JSON.stringify(req.headers)).authorization;
        var role = apivault.findApiKey(apikey, (key) => {
            (key.role === config.user_roles.ROLE_ROOT ||
             key.role === config.user_roles.ROLE_ADMIN || 
             key.role === config.user_roles.ROLE_TECH) ? next() : res.status(403).send('Forbidden').end(); 
        });
    },

    /**
     * User role authorization
     */
    userAuthorization(req, res, next) {
        var apikey = JSON.parse(JSON.stringify(req.headers)).authorization;
        var role = apivault.findApiKey(apikey, (key) => {
            (key.role === config.user_roles.ROLE_ROOT ||
             key.role === config.user_roles.ROLE_ADMIN || 
             key.role === config.user_roles.ROLE_TECH || 
             key.role === config.user_roles.ROLE_USER) ? next() : res.status(403).send('Forbidden').end(); 
        });
    }
}