/*
 * RTMS - API
 */

var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var app = express();
var cors = require('cors');
var server = require('http').Server(app);
var serverSocket = require('socket.io')(server);
var socketService = require('./socket.js');
var api = require('./api-routes.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/www/rtms/dist'));
app.use(cors());
app.use('/api', api);
app.get('*', (req, res) => {
    res.sendFile(__dirname + '/www/rtms/dist/index.html');
});

serverSocket.on('connection', function(socket) {
    console.log('Socket connected on port 8000');
    socketService.setSocket(socket).echo();
    socketService.setSockets(serverSocket.sockets);
});

server.listen(8000, () => {
    console.log('Listening on port 8000');
});