/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

module.exports = {
    mongoURI : 'mongodb://rtms:banz@mongodb51159-rmts-sandbox.unicloud.pl/rtms',
    // mongoURI : 'mongodb://root:root@localhost:27017/rtms',
    sensor_required_properties: [
        {field : 'name', error: 'Nazwa jest wymagana'},
        {field : 'type', error: 'Typ jest wymagany'},
        {field : 'localization', error: 'Lokalizacja jest wymagana'}
    ],
    user_create_required_properties: [
        {field: 'firstname', error: 'Imię jest wymagane'},
        {field: 'lastname', error: 'Nazwisko jest wymagane'},
        {field: 'email', error: 'Email jest wymagany'},
        {field: 'password', error: 'Hasło jest wymagane'},
        {field: 'role', error: 'Rola jest wymagana'},
    ],
    user_create_unique_fields: { field: 'email', error: 'Użytkownik o podanym adresie email istnieje' },
    user_update_required_properties: [
        {field: 'firstname', error: 'Imię jest wymagane'},
        {field: 'lastname', error: 'Nazwisko jest wymagane'},
        {field: 'email', error: 'Email jest wymagany'},
        {field: 'role', error: 'Rola jest wymagana'}
    ],
    user_login_required_properties: [
        {field: 'email', error: 'Email jest wymagany'},
        {field: 'password', error: 'Hasło jest wymagane'}
    ],
    user_login_errors: {
        NOTFOUND: { field: 'email', error: 'Użytkownik o podanym adresie email nie istnieje'},
        PASSWORD_INCORRECT: { field: 'password', error: 'Hasło nieprawidłowe' }
    },
    alert_type: {
        MAIL: 'email',
        SMS: 'sms'
    },
    user_roles: {
        ROLE_ROOT: 'ROLE_ROOT', 
        ROLE_ADMIN: 'ROLE_ADMIN',
        ROLE_TECH: 'ROLE_TECH',
        ROLE_USER: 'ROLE_USER'
    },
    authorization_routes_excluded : ['/api/login']
};
