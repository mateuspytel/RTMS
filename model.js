var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SchemaTypes = mongoose.Schema.Types;

/*
*   MongoDB documents schemas
*/
module.exports = {
    // User database schema
    User : mongoose.model('User', new Schema(
        {
            'email' : String,
            'password' : String,
            'firstname' : String,
            'lastname' : String,
            'created' : SchemaTypes.Number,
            'lastLogin' : SchemaTypes.Number,
            'active' : Boolean,
            'role' : String,
            'apikey' : {
                key: String,
                expirydate: SchemaTypes.Number
            }
        }
    )),
    // Sensor database schema
    Sensor : mongoose.model('Sensor', new Schema(
        {
            'serial_number' : String,
            'registered_at': SchemaTypes.Number,
            'type' : String,
            'name' : String,
            'localization' : String,
            'active_since' : SchemaTypes.Number,
            'active' : { type: Boolean, default: false },
            'lastmodified' : SchemaTypes.Number,
            'data' : [
                {
                    '_id': false,
                    'date' : SchemaTypes.Number,
                    'value' : SchemaTypes.Number
                }
            ],
            'alerts' : [
                {
                    'name' : String,
                    'alert_type' : String,
                    'active': { type: Boolean, default: true },
                    'threshold' : SchemaTypes.Number,
                    'receivers_list' : []
                }
            ]
        }
    )),
    AlertLog : mongoose.model('AlertLog', new Schema(
        {
            'sensor_id' : String,
            'sensor_type' : String,
            'localization' : String,
            'alert_name' : String,
            'alert_type' : String,
            'threshold' : SchemaTypes.Number,
            'cancelled': { type: Boolean, default: false },
            'date' : SchemaTypes.Number,
            'receivers_list' : [String]
        }
    )),
    // API log
    Log : mongoose.model('Log', new Schema( 
        {
            'activity' : [
                {
                    '_id': false,
                    'date' : SchemaTypes.Number,
                    'route' : String,
                    'ip' : String,
                    'authorization': String
                }
            ]
        }
    )),
    //Api key model
    ApiKey: mongoose.model('Apikey', new Schema(
        {
            'role': String,
            'apikey': String,
            'expirydate': SchemaTypes.Number
        }
           
    ))
}