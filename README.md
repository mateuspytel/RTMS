# Real-time Measurement System

Cloud based system for collecting and processing real time sensors data

## Technology stack

Tools used are follow: 
* MongoDB
* NodeJS
* Mongoose 
* Angular 5
* Socket.IO

## Sensors type 

Following sensors types are now available to track
* Voltage sensor
* Temperature sensor
* Humidity sensor
* Smoke sensor
* Flood sensor

## Installation
Install server REST-API: 
```sh
npm install
node main
```

Build Angular 5 web-app: 
```sh
cd www/rtms
npm install
ng build --prod --aot
```

To run Angular 5 development server 
```sh
ng serve --aot
```
