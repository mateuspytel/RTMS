var dao = require('./dao.js');

module.exports = {
    /**
     * Returns calculated array of chart data
     * @param {string[]} sensors 
     * @param {number} date_from 
     * @param {number} date_to 
     */
    getChartData(sensors, date_from, date_to) {
        return new Promise((resolve, reject) => {
            this.getSensorsDataPromise(sensors, date_from, date_to)
            .then( sensor_arr  => {
                this.chooseChartDataScenerio(sensor_arr).then( formated_data => {
                    resolve(formated_data);
                });
            })
            .catch(() => {});
        }); 
    },

    /**
     * Retrieves data for all selected sensors from database of selected date range
     * @param {string[]} sensors 
     * @param {number} from 
     * @param {number} to 
     */
    getSensorsDataPromise(sensors, from, to) {
        return new Promise((resolve, reject) => {
            var sensors_arr = [];
            var sensors_names_arr = [];
            for (var i=0; i<sensors.length; i++) {
                dao.getSensorsDataFromRange(sensors[i], from, to, (err, sensor) => {
                   sensors_arr.push(sensor.data);
                   sensors_names_arr.push(sensor.name);
                   (sensors_arr.length === sensors.length) ? 
                   resolve({ names: sensors_names_arr, sensors: sensors_arr }) : null;
                });
            }
        });
    },

    /**
     * Checks scenerio for selected data
     * SCENERIO -  
     * 0 : All sensors data array are equal in length - Best scenerio
     * 1 : Some sensors data array are not equal in length - not great 
     * @param {object[]} sensors_data
     */
    chooseChartDataScenerio(sensors_data) {
        return new Promise((resolve, reject) => {
            var scenario = sensors_data.sensors.checkChildEquality();
            if (scenario) {
                // If all selected sensors have same amount of data - prepare data table and finish
                this.prepareFormatedData(sensors_data).then( sensors => {
                    resolve(sensors);
                });
            }else { // If selected sensors have diffrent amount of data - interpolate data to fullfill data array
                var sensor_array = sensors_data.sensors;
                // Create data table header
                var sensor_name_array = sensors_data.names;
                // Names vector
                var names_vector = ['Data'];
                // Get index of largest child array
                var largest_array_index = sensor_array.getLargestElementIndex();
                names_vector.push(sensor_name_array[largest_array_index]);
                // Largest child array length
                var largest_array_length = sensor_array[largest_array_index].length;
                // Fullfill initial array with data of sensor with biggest data set
                this.prepareSingleSensorData(sensor_array[largest_array_index])
                .then( initial_array => {
                    // Remove already prepared sensor from array and proceed
                    sensor_array.splice(largest_array_index, 1);
                    sensor_name_array.splice(largest_array_index, 1);
                    // Fill missing data gaps
                    sensor_array.forEach( (sensor, index) => {
                        this.fillGaps(sensor, largest_array_length).then( filled_array => {
                            this.joinTables(initial_array, filled_array).then( joined_array => {
                                names_vector.push(sensor_name_array[index]);
                                initial_array = joined_array;
                                if (index === sensor_array.length - 1) {
                                    initial_array.unshift(names_vector);
                                    resolve(initial_array);
                                }                 
                            });
                        });         
                    });
                    
                });
            }
        });
    },

    /**
     * Prepares final data from all sensors that have same data array length
     * @param {[object[]]} sensors_arr 
     */
    prepareFormatedData(sensors_arr) {
        return new Promise((resolve, reject) => {
            var data_array = [];
            this.appendHeader(sensors_arr.names).then( header => {
                data_array.push(header);
                for( var i=0; i<sensors_arr.sensors[0].length; i++) {
                    for (var j=0; j<sensors_arr.sensors.length; j++) {
                        var vector = [];
                        var part_array = sensors_arr.sensors[j];
                        (j===0) ? vector.push(convertTimestampToDateFormat(part_array[i].date)) : null;
                        vector.push(part_array[i].value);
                        data_array.push(vector);
                        (data_array.length === sensors_arr.sensors.length + 1) ? resolve(data_array) : null;  
                    }
                }
            });
        });
    },


    /**
     * Prepares initial data from single sensor
     * @param {object[]} sensor_data 
     */
    prepareSingleSensorData(sensor_data) {
        return new Promise((resolve, reject) => {
            var data_arr = [];
            for (let i=0; i<sensor_data.length; i++) {
                data_arr.push([convertTimestampToDateFormat(sensor_data[i].date), sensor_data[i].value]);
                (data_arr.length === sensor_data.length) ? resolve(data_arr) : null;
            }
        });
    },

    /**
     * Fills gaps within sensor data 
     * @param {object[]} sensor_data
     */
    fillGaps(sensor_data, largest_element_length) {
        var gaps_count = Math.abs(largest_element_length - sensor_data.length);
        var gap_spread = Math.floor(sensor_data.length / gaps_count);
        return new Promise((resolve, reject) => {
            var filled_array = [];
            for (let i=0; i<sensor_data.length; i++) {
                filled_array.push(sensor_data[i].value);
            }
            for( let j=0; j<gaps_count; j++) {
                filled_array.splice((j * gap_spread), 0, -1);
                if (filled_array.length === largest_element_length) {
                    this.interpolateMissingSensorData(filled_array)
                    .then( correct_arr => {
                        resolve(correct_arr);
                    })
                }
            }
        });
    },

    /**            console.log('Parent length: ' + parent_table.length);
     * Interpolates values from gaps in sensor data array
     * @param {number[]} dataArray
     */
    interpolateMissingSensorData(dataArray) {
        return new Promise((resolve, reject) => {
            var ready = false;
            var correct_arr = dataArray;
            while(!ready) {
                for(let i=0; i<correct_arr.length; i++) {
                    ready = true;
                    if (correct_arr[i] === -1) {
                        correct_arr[i] = correct_arr.getAverageOfSiblings(i);
                        ready = false;
                        break;
                    }
                }
            }
            if (ready) resolve(correct_arr);
        });
    },

    /**    console.log('Child equality: ' + (length_arr.every( elem => elem === length_arr[0])));
     * Joins initial created table with sensor interpolated array
     * @param {[object[]]} parent_table 
     * @param {number[]} join 
     */
    joinTables(parent_table, join) {
        return new Promise((resolve, reject) => {convertTimestampToDateFormat
            var joined_table = parent_table;
            for (var i=0; i<parent_table.length; i++) {
                joined_table[i].push(join[i]);
                (i === parent_table.length - 1) ? resolve(joined_table) : null;
            }
        });
    },

    /**
     * Appends header array for selected sensors
     * @param {string[]} sensors_names
     */
    appendHeader(sensors_names) {
        return new Promise((resolve, reject) => {
            var header = ['Data'];
            for(var i=0; i<sensors_names.length; i++) {
                header.push(sensors_names[i]);
                (header.length === sensors_names.length + 1) ? resolve(header) : null;
            }
        });
    }
}

/**
 * Checks if child arrays have same length
 */
Array.prototype.checkChildEquality = function() {
    var length_arr = [];
    this.forEach( key => length_arr.push(key.length));
    return (length_arr.every( elem => elem === length_arr[0]));
}

/**
 * Retrieves largest child array
 */
Array.prototype.getLargestElementIndex = function() {
    var index = 0, prev_length = 0;
    this.forEach( (key, i) => { 
        if (key.length > prev_length) { index = i; prev_length = key.length; }
    });
    return index;
}

/**
 * Returns average from siblings elements in array
 * @param {number} startIndex
 */
Array.prototype.getAverageOfSiblings = function(startIndex) {
    var min = 0, max = 0;
    for (let i=startIndex; i<this.length; i++) {
        if (this[i] !== -1) { min = this[i]; break; }
    }
    for(let j=startIndex; j>0; j++) {
        if (this[j] !== -1) { max = this[j]; break; }
    }
    return (min + max) / 2;
}

/**
 * Converts numeric timestamp to well formated date
 * @param {number} timestamp 
 */
var convertTimestampToDateFormat = function(timestamp) {
    var date = new Date(timestamp);
    return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).substr(-2) + '-' + ('0' + date.getDate()).substr(-2) + ' ' 
            + ('0' + date.getHours()).substr(-2) + ':' + ('0' + date.getMinutes()).substr(-2);
}