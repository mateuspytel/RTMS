module.exports = {

    socket : {},
    sockets: {},

    /**
     * Opens Sockets ports
     * @param socket express app
     */
    echo() {
        this.socket.emit('index', {msg: 'Test message!'});
    },

    setSocket(socket) {
        this.socket = socket;
        return this;
    },

    setSockets(sockets) {
        this.sockets = sockets;
    },

    /**
     * Emits sensor's live data to socket channel
     * @param {string} id Sensor id 
     * @param {number} date Date of insertion
     * @param {number} value Value to push
     */
    sendSensorData(id, date, value) {
        if (Object.keys(this.socket).length !== 0) this.socket.emit(id, {_id: id, date: date, value: value});
    },

    /**
     * Emits sensor's live data to everyone
     * @param {string} id Sensor id 
     * @param {number} date Date of insertion
     * @param {number} value Value to push
     */
    broadcastSensorData(id, date, value) {
        if(Object.keys(this.sockets).length !== 0) this.sockets.emit(id, {_id: id, date: date, value: value});
    },

    /**
     * Emits alert to socket channel
     * @param {object} alert 
     */
    broadcastAlert(alert) {
        if (Object.keys(this.sockets).length !== 0) this.sockets.emit('alert', {alert: alert});
    }
}