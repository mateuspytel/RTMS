"use strict"
var mongoose = require('mongoose');
var model = require('./model.js');
var hash = require('crypto');
var config = require('./config.js');
var apikeyvault = require('./apikeyvault.js');

/*
*   Connect to MongoDB
*/
mongoose.Promise = global.Promise;
mongoose.connect(config.mongoURI)
.then( () => {
    console.log('Successfully connected to MongoDB');
}
,err => {
    console.log('Error. Connection with MongoDB failed: Error: ' + err);
});

module.exports = {
    /**
     * Saves new user in MongoDB and returns newly created object and error via callback function
     * @param {object} u user
     * @param {function} callback
     */
    createUser(u, callback) {
        var user = {
            email: u.email,
            password: hash.createHmac('sha256', u.password.toString()).digest('hex').toString(),
            firstname: u.firstname,
            lastname: u.lastname,
            active: true,
            created: Date.now(),
            role: u.role
        }

        model.User.findOne({email: user.email}, (err, result) => {
            if (!err && !result) { 
                model.User.create(user, (err, result) => {
                callback(err, result);
                }); 
            }else {
                callback({email: true});
            }
        });
    },

    /**
     * Removes user from database by marking user flag on 'inactive' in Mongo
     * @param {string} user_id
     * @param {function} callback
     */
    blockUser(user_id, callback) {
        model.User.findByIdAndUpdate(user_id, {$set: {active : false}}, {new: true}, function(err, user) {
            callback(err, user);
        });
    },

    /**
     * Unblocks user
     * @param {string} _id
     * @param {function} callback
     */
    unblockUser(user_id, callback) {
        model.User.findByIdAndUpdate(user_id, {$set: {active : true}}, (err, user) => {
            callback(err, user);
        });
    },

    /**
     * Retrieves user details
     * @param {string} id
     * @param {function} callback
     */
    getUserDetails(id, callback) {
        model.User.findOne({_id: id}, 'firstname lastname email created lastLogin active role', (err, user) => {
            callback(err, user);
        });
    },


    /**
     * Get user list
     */
    getUserList(callback) {
        model.User.find({}, '_id email firstname lastname created lastLogin active role', (err, users) => {
            callback(err, users);
        });
    },

    /**
     * Updates user
     * @param {object} user
     * @param {function} callback
     */
    updateUser(user, callback) {
        model.User.update({_id : user._id}, {
            $set: {
                email: user.email,
                firstname: user.firstname,
                lastname: user.lastname,
                role: user.role,
                active: user.active
            }
        }, (err, res) => {
                    callback(err, res);
});
    },

    /**
     * Signs in user
     * @param {object} user
     * @param {function} callback
     */
    login(user, callback) {
        model.User.findOne({email: user.email, active: true}, '_id email password', (err, u) => {
            if (!err && u) {
                console.log('Hash password: ' + hash.createHmac('sha256', user.password.toString()).digest('hex').toString());
                console.log('Database password: ' + u.password);
                (hash.createHmac('sha256', user.password.toString()).digest('hex').toString() !== u.password) ?
                callback(config.user_login_errors.PASSWORD_INCORRECT) : callback(null, u);  
            } else {
                callback(config.user_login_errors.NOTFOUND);
            }
        });
    },

    /**
     * Signs out user
     * @param {string} key
     * @param {function} callback
     */
    logout(key, callback) {
        model.User.updateOne({ "apikey.key" : key}, {$unset: {apikey: ''}}, (err, result) => {
            callback(err, result);
        });
    },

    /**
     * Saves APIKey in user document
     * @param {string} id User ID
     * @param {function} callback
     */
    generateAndSaveAPIKey(id, callback) {
        var key = this.generateAPIKEY();
        var expirydate = new Date().getTime() + (1000 * 60 * 60 * 24);
        model.User.findOneAndUpdate({_id : id}, 
            {$set: {
                lastLogin: Date.now(),
                apikey: {
                    key: key,
                    expirydate: expirydate
                }
            }}, {new: true}, (err, user) => {
            if (!err && user) apikeyvault.addApiKey({key: user.apikey.key, expirydate: user.apikey.expirydate, role: user.role}); 
            callback(err, {_id: user._id, firstname: user.firstname, lastname: user.lastname, role: user.role, apikey: user.apikey.key});
        });
    },

    /**
     * Saves new user in MongoDB and returns newly created object and error via callback function
     * @param {object} sensor
     * @param {function} callback
     */
    createSensor(sensor, callback) {
        model.Sensor.create(sensor, (err, s) => {
            callback(err, s);
        });
    },

    /**
     * Removes sensor from DB
     * @param {string} id sensor id
     * @param {function} callback
     */
    removeSensor(id, callback) {
        model.Sensor.remove({_id: id}, (err) => {
            callback(err);
        });
    },

    /**
     * Return sensors list
     * @param {function} callback
     */
    getSensorsList(callback) {
        model.Sensor.find({}, '_id serial_number registered_at active_since type name localization active lastmodified', (err, sensors) => {
            callback(err, sensors);
        });
    },

    /**
     * Return sensors details without data and alerts fields
     * @param {string} sensor_id
     * @param {function} callback
     */
    getSensorsDetails(sensor_id, callback) {
        model.Sensor.findOne( { _id : sensor_id },
            '_id registered_at type name localization active active_since lastmodified alerts', (err, sensor) => {
            callback(err, sensor);
        });
    },
    
    /**
     * Return only sensors data 
     * @param {string} sensor_id
     * @param {function} callback
     */
    getSensorsData(sensor_id, callback) {
        model.Sensor.findOne({_id : sensor_id}, '_id data lastmodified active_since alerts localization active name type', (err, sensor) => {
            if (sensor) {
                sensor.data = sensor.data.filter( dat => (dat.date >= (sensor.lastmodified-(1000*60*60*24)) && dat.date <= sensor.lastmodified));
            }
            callback(err, sensor);
        });
    },

    /**
     * Return only sensors data but from time range
     * @param {string} sensor_id
     * @param {number} timestamp_from
     * @param {number} timestamp_to
     * @param {function} callback
     */
    getSensorsDataFromRange(sensor_id, timestamp_from, timestamp_to, callback) {
        model.Sensor.findOne(
            {
                "_id" : sensor_id
            }, '_id name data', (err, sensor) => {
                if (sensor) {
                    sensor.data = sensor.data.filter( dat => (dat.date >= timestamp_from && dat.date <= timestamp_to));
                }
                callback(err, sensor);
            });
    },

    /**
     * Get alert state of sensor
     * @param {string} sensor_id
     * @param {function} callback
     */
    getSensorsAlerts(sensor_id, callback) {
        model.Sensor.findOne({_id: sensor_id}, '_id alerts', 
            (err, alert) => {
                callback(err, alert);
            });
    },

    /**
     * Update sensors data
     * @param {string} sensor_id
     * @param {function} callback
     */
    updateSensorsData(sensor_id, value, callback) {
        model.Sensor.findOneAndUpdate({_id : sensor_id, active: true}, 
            {
                $addToSet: { data: {date: Date.now(), value: value}},
                $set: {lastmodified : Date.now()},
            }, {new: true}, (err, sensor) => {
                if (sensor && sensor.data && sensor.data.length > 0) {
                    model.Sensor.findOneAndUpdate({_id: sensor_id, active_since: {$exists : false}},
                    {
                        $set: {active_since: sensor.data[0].date}
                    }, {new: true}, (err, s) => {});
                }
                
                callback(err, sensor);
            });
    },

    /**
     * Update anonymously sensor data
     * @param {string} serial_number
     * @param {number} value 
     * @param {function} callback
     */
    updateAnonymouslySensorsData(serial_number, value, callback) {
        model.Sensor.findOneAndUpdate({ serial_number: serial_number }, 
            {
                $setOnInsert: { active : false }
            }, { upsert: true, new: true }, (err, sensor) => {
                if (sensor) {
                    model.Sensor.update({serial_number : sensor.serial_number, active: true}, 
                    {
                        $addToSet : {data : {date : Date.now(), value: value}},
                        $set: {lastmodified : Date.now()}
                    }, {new: true}, (err, s) => {});
                }
                if (!sensor.hasOwnProperty("active_since") && sensor.data && sensor.data.length > 0) 
                model.Sensor.update({serial_number : serial_number}, {$set: {active_since: sensor.data[0].date}},
                (err, s) => {});

                if (sensor && !sensor.active) { callback(true, sensor); }
                else callback(err, sensor);
        });
    },

    /**
     * Updates sensors details
     * @param {object} sensor
     * @param {function} callback
     */
    updateSensorsDetails(sensor, callback) {
        model.Sensor.findOneAndUpdate({_id : sensor._id},
        {
            $set : {
                type : sensor.type,
                name : sensor.name,
                localization : sensor.localization,
                active : sensor.active,
                alerts: sensor.alerts
            }
        }, 
        {
            new: true,
            fields: {'_id' : 1, 'registered_at' : 1, 'type' : 1, 'localization' : 1, 'active_since' : 1, 'lastmodified' : 1,
            'alerts' : 1}
        }
        ,(err, sensor) => {
            callback(err, sensor);
        });
    },

    /**
     * Updates sensors alert flag
     * @param {string} id sensor id
     * @param {string} alert_name alert name
     * @param {boolean} active sensor alert flag
     * @param {function} callback 
     */
    updateSensorsAlertState(id, alert_name, active, callback) {
        model.Sensor.findOneAndUpdate(
            { '_id': id, 'alerts.name' : alert_name },
            { $set: { 'alerts.$.active': active }},
            (err, sensor) => { if(callback) callback(err, sensor); });
    },

    /**
     * Set alert on current sensor
     * @param {object} alert
     * @param {function} callback
     */
    updateAlert(alert, callback) {
        model.Sensor.update({_id : alert.sensor_id}, 
            {
                $addToSet : { alerts: 
                    {
                        type: alert.alerts.type,
                        name: alert.alerts.name, 
                        threshold: alert.alerts.threshold,
                        receivers_list : alert.alerts.receivers_list
                    } 
                }
            }, 
        (err, sensor) => {
            callback(err, sensor);
        });
    },

    /**
     * Logs sensor's alert
     * @param {string} sensor_id 
     * @param {string} sensor_type
     * @param {string} localization 
     * @param {sting} alert_name
     * @param {string} alert_type 
     * @param {number} threshold
     * @param {string[]} receivers_list 
     * @param {function} callback 
     */
    logAlert(sensor_id, sensor_type, localization, alert_name, alert_type, threshold, receivers_list, callback) {
        var alert = {
            sensor_id : sensor_id,
            sensor_type : sensor_type,
            localization : localization,
            alert_name : alert_name,
            alert_type: alert_type,
            threshold : threshold,
            date : Date.now(),
            receivers_list : receivers_list
        }
        model.AlertLog.create(alert, (err, saved_alert) => {
            callback(err, saved_alert);
        });
    },

    /**
     * Retrieves all alert logs
     * @param {function} callback
     */
    getAlertLogs(callback) {
        model.AlertLog.find({}, (err, logs) => {
            callback(err, logs);
        });
    },

    /**
     * Retrieves active alerts of sensor
     * @param {string} id Sensor id
     * @param {function} callback
     */

     getActiveAlerts(id, callback) {
         model.AlertLog.find({sensor_id : id, cancelled : false}, (err, alerts) => {
            callback(err, alerts);
         });
     },

     /**
      * @param {string} id Alert id
      * @param {function} callback
      */
      cancelAlert(id, callback) {
          model.AlertLog.findOneAndUpdate({_id: id}, {$set: {cancelled: true}},
            (err, alertlog) => {
                callback(err, alertlog);
          });
      },

    /**
     * Deletes alert by id
     * @param {string} sensor_id
     * @param {string} id
     * @param {function} callback
     */
    deleteAlert(sensor_id, id, callback) {
        model.Sensor.update({_id: sensor_id}, {$pull: {alerts: {_id: id}}}, (err, result) => {
            callback(err, result);
        });
    },

    /**
     * Finds sensor by name
     * @param {string} phrase
     * @param {function} callback
     */
    findSensor(phrase, callback) {
        model.Sensor.find({name: {$regex: phrase}}, '_id name type localization', (err, sensor) => {
            callback(err, sensor);
        });
    },
    

    /**
     * Log activity on API routes
     * @param {string} ip_adress
     * @param {string} route 
     * @param {function} callback
     */ 
    logActivity(ip_adress, route, authorization, callback) {
        model.Log.update({}, { $addToSet: { 
            activity: {
                date: Date.now(),
                route: route,
                ip: ip_adress,
                authorization: authorization
            }
        }}, {upsert: true, new: true},
        (err, log) => {
            callback(err, log);
        });
    },
    /**
     * Get Authorization API key 
     * @param {string} role
     */
    getAuthorizationAPIKey(role, callback) {
        model.ApiKey.findOne({role: role}, (err, apikey) => {
            callback(err, apikey);
        });
    },
    
    /**
     * Generate APIKEY
     */
    generateAPIKEY() {
        var seed = Math.random().toString(36).substr(2, 5);
        return hash.createHmac('sha256', seed).digest('hex').toString();
    }
};